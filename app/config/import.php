<?php

/**
 * stores the import configurations
 * @author James Makau <jacjimus@gmail.com>
 * @since 1.0
 * @version 1.0
 * @package bluebound
 */
return array(
    'application.models.*',
    'application.models.forms.*',
    'application.components.*',
    'application.components.widgets.*',
    'ext.easyimage.EasyImage',
    //user module
    'application.modules.users.models.*',
    'application.modules.users.components.*',
    'application.modules.users.components.behaviors.*',
    
    'application.modules.settings.models.*',
    'application.modules.settings.components.*',
    //Access Codes module
    'application.modules.accesscodes.models.*',
    'application.modules.accesscodes.components.*',
    //auth module
    'application.modules.auth.models.*',
    'application.modules.auth.components.*',
    //Exams module
    'application.modules.exams.models.*',
    'application.modules.exams.components.*',
    //System Logs module
    'application.modules.setup.models.*',
    'application.modules.setup.components.*',
    //reports module
    'application.modules.reports.models.*',
    'application.modules.reports.components.*',
    
    //Front module
    'application.modules.front.models.*',
    'application.modules.front.components.*',
    // Chosen extension
    'application.extensions.chosen.*',
    
    // PHPExcel Library
    'application.vendors.phpexcel.PHPExcel',
    'ext.yiireport.*',
    
);

/**
 * LineItem Script
 * @author Fred <mconyango@gmail.com>
 * Timestamp: 2nd Jun 2014 at 1803hr
 */

var MyLineItem = {
    optionsDefault: {
        selectors: {
            container: undefined,
            itemsTable: undefined,
            addNewItemOnSave: undefined,
            headerIdField: undefined,
            addItem: undefined,
            cancel: undefined,
            submit: undefined,
            notification: undefined,
            itemIdField: undefined,
            itemHeaderIdField: undefined,
            saveItem: '.save-line-item',
            deleteItem: '.delete-line-item',
            item: 'tr.line-item',
        },
        saveItemUrl: undefined,
        deleteItemUrl: undefined,
        beforeSave: function (tr, settings) {
        },
        afterSave: function (tr, response, settings) {
        },
        beforeDelete: function (tr, settings) {
        },
        afterDelete: function (tr, response, settings) {
        },
        beforeFinish: function (e, settings) {
        },
        afterFinish: function (response, settings) {
            $(settings.selectors.container).html(response.html);
            MyUtils.showAlertMessage(response.message, 'success', settings.selectors.notification);
        },
    },
    init: function (options) {
        'use strict';
        var settings = $.extend({}, this.optionsDefault, options || {});
        this.addItem(settings);
        this.cancel(settings);
        this.finish(settings);
        this.deleteItem(settings);
        this.saveItem(settings);
    },
    addItem: function (settings) {
        var add_item = function (e) {
            var url = $(e).data('href');
            var index = $(settings.selectors.container + ' ' + settings.selectors.item).length + 1;
            $.ajax({
                type: 'POST',
                url: url,
                data: 'index=' + index,
                success: function (html) {
                    var container = $(settings.selectors.container + ' ' + settings.selectors.itemsTable).find('tbody');
                    container.append(html);
                }
            });
        };
        //onclick
        $(settings.selectors.container).on('click', settings.selectors.addItem, function (e) {
            e.preventDefault();
            add_item(this);
        });
    },
    cancel: function (settings) {
        var cancel = function (e) {
            var url = $(e).data('href')
                    , id = $(settings.selectors.headerIdField).val();
            $.ajax({
                type: 'POST',
                url: url,
                data: 'id=' + id,
                success: function (html) {
                    $(settings.selectors.container).html(html);
                }
            });
        };
        //on click
        $(settings.selectors.container).on('click', settings.selectors.cancel, function (e) {
            e.preventDefault();
            cancel(this);
        });
    },
    finish: function (settings) {
        var submit = function (e) {
            settings.beforeFinish(e, settings);
            var url = $(e).data('href')
                    , data = $(settings.selectors.container).find('form').serialize();
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        settings.afterFinish(response, settings);
                    } else {
                        MyUtils.display_model_errors(response.message, false, true);
                    }
                },
                beforeSend: function () {
                    MyUtils.startBlockUI();
                },
                complete: function () {
                    MyUtils.stopBlockUI();
                },
                error: function (XHR) {
                    console.log(XHR.responseText);
                }
            });
        };
        //on click
        $(settings.selectors.container).on('click', settings.selectors.submit, function (e) {
            e.preventDefault();
            submit(this);
        });
    },
    saveItem: function (settings) {
        var input_error_class = 'my-form-error';
        var show_error = function (tr, input_class) {
            tr.find('.' + input_class).addClass(input_error_class);
            tr.addClass('bg-danger');
        };
        var hide_error = function (tr) {
            tr.find('.' + input_error_class).removeClass(input_error_class);
            tr.removeClass('bg-danger');
        };
        var saved_css_class = 'text-success'
                , unsaved_css_class = 'text-warning';
        var mark_as_saved = function (tr) {
            tr.find(settings.selectors.saveItem).removeClass(unsaved_css_class).addClass(saved_css_class);
        };
        var post = function (e) {
            var tr = $(e).closest('tr');
            settings.beforeSave(tr, settings);
            //set the parent_id of the row
            var head_id_field = $(settings.selectors.headerIdField);
            if (MyUtils.empty(tr.find(settings.selectors.itemHeaderIdField).val())) {
                $(settings.selectors.itemHeaderIdField).val(head_id_field.val());
            }
            var url = settings.saveItemUrl
                    , data = tr.find('input,select,textarea').serialize();
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        settings.afterSave(tr, response, settings);
                        if (!MyUtils.empty(response.data.head_id) && MyUtils.empty(head_id_field.val())) {
                            head_id_field.val(response.data.head_id);
                        }
                        hide_error(tr);
                        var add_row_on_save = $(settings.selectors.addNewItemOnSave).is(':checked');
                        if (add_row_on_save && MyUtils.empty(tr.find(settings.selectors.itemIdField).val())) {
                            $(settings.selectors.addItem).trigger('click');
                        }
                        tr.find(settings.selectors.itemIdField).val(response.data.id);
                        mark_as_saved(tr);
                    }
                    else {
                        hide_error(tr);
                        //show error
                        var jsonData = $.parseJSON(response.message);
                        $.each(jsonData, function (i) {
                            show_error(tr, i);
                        });
                        MyUtils.display_model_errors(response.message, false, true);
                    }
                },
                error: function (XHR) {
                    console.log(XHR.responseText);
                }
            });
        };
        //on click
        $(settings.selectors.container).on('click', settings.selectors.saveItem, function (e) {
            e.preventDefault();
            post(this);
        });
    },
    deleteItem: function (settings) {
        'use strict';
        var delete_item = function (e) {
            var tr = $(e).closest('tr');
            settings.beforeDelete(tr, settings);
            var url = settings.deleteItemUrl
                    , id = tr.find(settings.selectors.itemIdField).val();
            if (MyUtils.empty(id)) {
                tr.remove();
                return false;
            }
            $.ajax({
                type: 'POST',
                url: url,
                data: 'id=' + id,
                dataType: 'json',
                success: function (response) {
                    settings.afterDelete(tr, response, settings);
                    tr.remove();
                }
            });
        };
        //on click
        $(settings.selectors.container).on('click', settings.selectors.deleteItem, function (e) {
            e.preventDefault();
            var $this = this;
            var confirm_msg = $($this).data('delete-confirm');
            if (MyUtils.empty(confirm_msg)) {
                delete_item($this);
            } else {
                BootstrapDialog.confirm(confirm_msg, function (result) {
                    if (result) {
                        delete_item($this);
                    }
                })
            }
        });
    },
};



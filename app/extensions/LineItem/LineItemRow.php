<?php

/**
 * Description of LineItem
 *
 * @author Fred <mconyango@gmail.com>
 */
class LineItemRow extends CWidget
{

    /**
     *
     * @var \ActiveRecord
     */
    public $model;

    /**
     * One or More td
     * <pre>
     * <td>content</td>
     * </pre>
     * @var string
     */
    public $td;

    /**
     *
     * @var type
     */
    public $index = 1;

    /**
     *
     * @var type
     */
    public $primaryKeyField = 'id';

    /**
     * This is also header_id field
     * @var type
     */
    public $foreignKeyField;

    /**
     *
     * @var type
     */
    public $trHtmlOptions = array('class' => 'line-item');

    /**
     *
     * @var type
     */
    public $buttonsTemplate;

    /**
     *
     * @var type
     */
    public $buttonsHtml;

    /**
     *
     * @var type
     */
    public $showSaveButton = true;

    /**
     *
     * @var type
     */
    public $showDeleteButton = true;

    /**
     *
     * @var type
     */
    public $buttonsWrapperHtmlOptions = array('style' => 'min-width: 80px;text-align: center;');

    /**
     *
     * @var type
     */
    public $saveButtonHtmlOptions = array();

    /**
     *
     * @var type
     */
    public $deleteButtonHtmlOptions = array('class' => 'text-danger');

    /**
     *
     * @var type
     */
    public $saveButtonLabel = '<i class="fa fa-check-circle fa-2x"></i>';

    /**
     *
     * @var type
     */
    public $deleteButtonLabel = '<i class="fa fa-minus-circle fa-2x"></i>';

    /**
     *
     * @var type
     */
    private $saveCssClass = 'save-line-item';

    /**
     *
     * @var type
     */
    private $deleteCssClass = 'delete-line-item';

    /**
     *
     * @var type
     */
    private $modelClassName;

    public function init()
    {
        $this->modelClassName = get_class($this->model);
        parent::init();
    }

    public function run()
    {
        echo $this->getRowHtml();
    }

    protected function getRowHtml()
    {
        $template = '{{cells}}{{buttons}}';
        $this->setButtonsHtml();
        $this->trHtmlOptions['id'] = $this->modelClassName . '_' . $this->index;
        $row_contents = strtr($template, array(
            '{{cells}}' => $this->td,
            '{{buttons}}' => $this->buttonsHtml,
        ));
        return CHtml::tag('tr', $this->trHtmlOptions, $row_contents);
    }

    protected function setButtonsHtml()
    {
        if (null !== $this->buttonsHtml)
            return false;
        if (null === $this->buttonsTemplate)
            $this->buttonsTemplate = '{{save_button}}&nbsp;&nbsp;{{delete_button}}';
        $this->saveButtonHtmlOptions['title'] = Lang::t('Save');
        if (empty($this->saveButtonHtmlOptions['class']))
            $this->saveButtonHtmlOptions['class'] = $this->saveCssClass;
        else
            $this->saveButtonHtmlOptions['class'] .= ' ' . $this->saveCssClass;
        $save_button_class = $this->model->getIsNewRecord() ? 'text-warning' : 'text-success';
        $this->saveButtonHtmlOptions['class'].=' ' . $save_button_class;
        if (!$this->showSaveButton)
            $this->saveButtonHtmlOptions['class'].=' hidden';
        $save_button = CHtml::link($this->saveButtonLabel, 'javascript:void(0);', $this->saveButtonHtmlOptions);

        $this->deleteButtonHtmlOptions['title'] = Lang::t('Delete');
        if (empty($this->deleteButtonHtmlOptions['class']))
            $this->deleteButtonHtmlOptions['class'] = $this->deleteCssClass;
        else
            $this->deleteButtonHtmlOptions['class'].=' ' . $this->deleteCssClass;
        if (!$this->showDeleteButton)
            $this->deleteButtonHtmlOptions['class'].=' hidden';
        $delete_button = CHtml::link($this->deleteButtonLabel, 'javascript:void(0);', $this->deleteButtonHtmlOptions);

        $inner_html = strtr($this->buttonsTemplate, array(
            '{{save_button}}' => $save_button,
            '{{delete_button}}' => $delete_button,
        ));

        $inner_html.=$this->getHiddenFieldsHtml();
        $this->buttonsHtml = CHtml::tag('td', $this->buttonsWrapperHtmlOptions, $inner_html);
    }

    protected function getHiddenFieldsHtml()
    {
        $html = CHtml::activeHiddenField($this->model, $this->primaryKeyField, array('id' => "", 'class' => $this->modelClassName . '_' . $this->primaryKeyField));
        if (!empty($this->foreignKeyField))
            $html .= CHtml::activeHiddenField($this->model, $this->foreignKeyField, array('id' => "", 'class' => $this->modelClassName . '_' . $this->foreignKeyField));
        return $html;
    }

}

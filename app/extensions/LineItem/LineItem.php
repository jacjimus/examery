<?php

/**
 * Description of RenderHeader
 *
 * @author Fred <mconyango@gmail.com>
 */
class LineItem extends CWidget
{

    /**
     * The id of the html element containing this widget
     * @var type
     */
    public $container;

    /**
     *
     * @var type
     */
    public $model;

    /**
     *
     * @var type
     */
    public $content;

    /**
     *
     * @var type
     */
    public $itemsTableHtml;

    /**
     *
     * @var type
     */
    public $itemsTableHtmlOptions = array('class' => 'table table-bordered');

    /**
     *
     * @var type
     */
    public $cancelUrl;

    /**
     *
     * @var type
     */
    public $finishUrl;

    /**
     *
     * @var type
     */
    public $addItemUrl;

    /**
     *
     * @var type
     */
    public $itemsTitle;

    /**
     *
     * @var type
     */
    public $formTag = 'form';

    /**
     *
     * @var type
     */
    public $formHtmlOptions = array('class' => 'form-horizontal', 'role' => 'form');

    /**
     *
     * @var type
     */
    public $addItemOnSave = true;

    /**
     *
     * @var type
     */
    public $contentWrapperHtmlOptions = array();

    /**
     *
     * @var type
     */
    public $primaryKeyField = 'id';

    /**
     *
     * @var type
     */
    public $template = '{{notif}}{{content}}{{buttons}}';

    /**
     *
     * @var type
     */
    public $notifHtmlOptions = array();

    /**
     *
     * @var type
     */
    public $showPanel = true;

    /**
     *
     * @var type
     */
    public $showHead = true;

    /**
     *
     * @var type
     */
    public $showButtons = true;

    /**
     *
     * @var type
     */
    public $itemsTemplate;

    /**
     *
     * @var type
     */
    public $itemsContainerHtmlOptions = array();

    /**
     *
     * @var type
     */
    public $addItemLinkHtmlOptions = array('class' => '');

    /**
     *
     * @var type
     */
    public $addNewItemLabel;

    /**
     *
     * @var type
     */
    public $addItemHtmlOptions = array();

    /**
     *
     * @var type
     */
    public $addItemLabel;

    /**
     *
     * @var type
     */
    public $buttonsWrapperHtmlOptions = array('class' => 'well well-sm clearfix');

    /**
     *
     * @var type
     */
    public $buttonsTemplate;

    /**
     *
     * @var type
     */
    public $finishButtonHtmlOptions = array('class' => 'btn btn-primary');

    /**
     *
     * @var type
     */
    public $finishButtonLabel;

    /**
     *
     * @var type
     */
    public $cancelButtonHtmlOptions = array('class' => 'btn btn-default');

    /**
     *
     * @var type
     */
    public $cancelButtonLabel;

    /**
     *
     * @var type
     */
    private $model_class_name;

    /**
     *
     * @var type
     */
    public $beforeFinish;

    /**
     *
     * @var type
     */
    public $afterFinish;

    /**
     *
     * @var type
     */
    public $beforeSave;

    /**
     *
     * @var type
     */
    public $afterSave;

    /**
     *
     * @var type
     */
    public $beforeDelete;

    /**
     *
     * @var type
     */
    public $afterDelete;

    /**
     *
     * @var type
     */
    public $itemIdFieldSelector;

    /**
     *
     * @var type
     */
    public $itemHeaderIdFieldSelector;

    /**
     *
     * @var type
     */
    public $saveItemSelector;

    /**
     *
     * @var type
     */
    public $deleteItemSelector;

    /**
     *
     * @var type
     */
    public $itemSelector;

    /**
     *
     * @var type
     */
    public $saveItemUrl;

    /**
     *
     * @var type
     */
    public $deleteItemUrl;

    public function init()
    {
        if (empty($this->container))
            $this->container = 'line_item_form_wrapper';

        if (empty($this->formHtmlOptions['id']))
            $this->formHtmlOptions['id'] = 'line_items_form';
        $this->formHtmlOptions['method'] = 'post';
        if (!empty($this->model))
            $this->model_class_name = get_class($this->model);
        parent::init();
    }

    public function run()
    {
        $inner_html = $this->processTemplate();
        if (empty($this->formHtmlOptions['action']))
            $this->formHtmlOptions['action'] = $this->finishUrl;
        echo CHtml::tag($this->formTag, $this->formHtmlOptions, $inner_html);
        $this->registerScripts();
    }

    protected function processTemplate()
    {
        if (empty($this->template))
            $this->template = '{{notif}}{{content}}{{buttons}}';
        $html = strtr($this->template, array(
            '{{notif}}' => $this->getNotifHtml(),
            '{{content}}' => $this->getContentHtml(),
            '{{buttons}}' => ($this->showButtons) ? $this->getButtonsHtml() : '',
        ));
        return $html;
    }

    protected function getContentHtml()
    {
        if (empty($this->content))
            $this->content = '{{items}}';
        $content = strtr($this->content, array(
            '{{items}}' => $this->getItemsHtml(),
        ));
        $hidden_fields = $this->getHeadHiddenFieldHtml();
        $inner_html = $content . $hidden_fields;
        return CHtml::tag('div', $this->contentWrapperHtmlOptions, $inner_html);
    }

    protected function getButtonsHtml()
    {
        if (empty($this->model))
            return "";

        if (null === $this->buttonsTemplate) {
            $this->buttonsTemplate = '<div class="pull-right">{{finish}}&nbsp;&nbsp;{{cancel}}</div>';
        }
        $this->finishButtonHtmlOptions['type'] = 'button';
        if (empty($this->finishButtonHtmlOptions['id']))
            $this->finishButtonHtmlOptions['id'] = 'finish_line_item_button';
        if (empty($this->finishUrl))
            $this->finishUrl = $this->getOwner()->createUrl('finish');
        $this->finishButtonHtmlOptions['data-href'] = $this->finishUrl;
        if (empty($this->finishButtonLabel))
            $this->finishButtonLabel = Lang::t('Finish');

        $this->cancelButtonHtmlOptions['type'] = 'button';
        if (empty($this->cancelButtonHtmlOptions['id']))
            $this->cancelButtonHtmlOptions['id'] = 'cancel_line_item_button';
        if (empty($this->cancelUrl))
            $this->cancelUrl = $this->getOwner()->createUrl('cancel');
        $this->cancelButtonHtmlOptions['data-href'] = $this->cancelUrl;
        if (empty($this->cancelButtonLabel))
            $this->cancelButtonLabel = Lang::t('Cancel');

        $inner_html = strtr($this->buttonsTemplate, array(
            '{{finish}}' => CHtml::tag('button', $this->finishButtonHtmlOptions, $this->finishButtonLabel),
            '{{cancel}}' => CHtml::tag('button', $this->cancelButtonHtmlOptions, $this->cancelButtonLabel),
        ));
        return CHtml::tag('div', $this->buttonsWrapperHtmlOptions, $inner_html);
    }

    protected function getHeadHiddenFieldHtml()
    {
        if (!empty($this->model)) {
            if (empty($this->primaryKeyField))
                $this->primaryKeyField = 'id';
            return CHtml::activeHiddenField($this->model, $this->primaryKeyField);
        } else
            return "";
    }

    protected function getItemsHtml()
    {
        $this->itemsContainerHtmlOptions['class'] = 'panel panel-default';
        if (null === $this->itemsTemplate && $this->showPanel) {
            $this->itemsTemplate = '<div class="panel-heading"><h3 class="panel-title">{{items_title}}</h3></div>'
                    . '{{items_table}}'
                    . '<div class="panel-footer clearfix"><ul class="list-inline" style="text-align:right;"><li>{{auto_add_checkbox}}</li><li>&nbsp;&nbsp;&nbsp;</li><li>{{new_row_link}}</li></ul></div>';
        }
        $auto_add_checkbox = $this->getAutoAddCheckBoxHtml();
        $new_row_link = $this->getNewRowLinkHtml();
        if (empty($this->itemsTableHtmlOptions['id']))
            $this->itemsTableHtmlOptions['id'] = 'line_items_table';
        $inner_html = strtr($this->itemsTemplate, array(
            '{{items_title}}' => $this->itemsTitle,
            '{{items_table}}' => CHtml::tag('table', $this->itemsTableHtmlOptions, $this->itemsTableHtml),
            '{{auto_add_checkbox}}' => $auto_add_checkbox,
            '{{new_row_link}}' => $new_row_link,
        ));
        return CHtml::tag('div', $this->itemsContainerHtmlOptions, $inner_html);
    }

    protected function getAutoAddCheckBoxHtml()
    {
        if (!$this->addItemOnSave)
            return "";
        if (empty($this->addItemLabel))
            $this->addItemLabel = Lang::t('Auto add new row on save');
        $template = '{{checkbox}}&nbsp;&nbsp;<span class="text-muted">{{label}}</span>';
        if (empty($this->addItemHtmlOptions['id']))
            $this->addItemHtmlOptions['id'] = 'auto_add_new_row_on_save';
        $checkbox = CHtml::checkBox($this->addItemHtmlOptions['id'], $this->addItemOnSave, $this->addItemHtmlOptions);
        return strtr($template, array(
            '{{checkbox}}' => $checkbox,
            '{{label}}' => $this->addItemLabel,
        ));
    }

    protected function getNewRowLinkHtml()
    {
        if (empty($this->addItemLinkHtmlOptions['id']))
            $this->addItemLinkHtmlOptions['id'] = 'add_new_item_line';
        if (empty($this->addNewItemLabel))
            $this->addNewItemLabel = Lang::t('Add New Row');
        $this->addNewItemLabel .= ' <i class="fa fa-level-up"></i>';
        if (empty($this->addItemUrl))
            $this->addItemUrl = $this->getOwner()->createUrl('addItem');
        $this->addItemLinkHtmlOptions['data-href'] = $this->addItemUrl;

        return CHtml::link($this->addNewItemLabel, 'javascript:void(0);', $this->addItemLinkHtmlOptions);
    }

    protected function getNotifHtml()
    {
        if (empty($this->notifHtmlOptions['id']))
            $this->notifHtmlOptions['id'] = 'line_items_notif_wrapper';
        return CHtml::tag('div', $this->notifHtmlOptions, "");
    }

    protected function registerScripts()
    {
        $options = array(
            'selectors' => array(
                'container' => '#' . $this->container,
                'itemsTable' => !empty($this->itemsTableHtmlOptions['id']) ? '#' . $this->itemsTableHtmlOptions['id'] : null,
                'headerIdField' => !empty($this->model_class_name) ? '#' . $this->model_class_name . '_' . $this->primaryKeyField : null,
                'addItem' => !empty($this->addItemLinkHtmlOptions['id']) ? '#' . $this->addItemLinkHtmlOptions['id'] : null,
                'cancel' => '#' . $this->cancelButtonHtmlOptions['id'],
                'submit' => !empty($this->finishButtonHtmlOptions['id']) ? '#' . $this->finishButtonHtmlOptions['id'] : null,
                'notification' => !empty($this->notifHtmlOptions['id']) ? '#' . $this->notifHtmlOptions['id'] : null,
                'addNewItemOnSave' => isset($this->addItemHtmlOptions['id']) ? '#' . $this->addItemHtmlOptions['id'] : null,
                'itemIdField' => $this->itemIdFieldSelector,
                'itemHeaderIdField' => $this->itemHeaderIdFieldSelector,
                'saveItem' => !empty($this->saveItemSelector) ? $this->saveItemSelector : '.save-line-item',
                'deleteItem' => !empty($this->deleteItemSelector) ? $this->deleteItemSelector : '.delete-line-item',
                'item' => !empty($this->itemSelector) ? $this->itemSelector : 'tr.line-item',
            ),
            'saveItemUrl' => !empty($this->saveItemUrl) ? $this->saveItemUrl : $this->getOwner()->createUrl('saveItem'),
            'deleteItemUrl' => !empty($this->deleteItemUrl) ? $this->deleteItemUrl : $this->getOwner()->createUrl('deleteItem'),
        );
        foreach (array('beforeFinish', 'afterFinish', 'afterSave', 'beforeSave', 'beforeDelete', 'afterDelete') as $event) {
            if ($this->$event !== null) {
                if ($this->$event instanceof CJavaScriptExpression)
                    $options[$event] = $this->$event;
                else
                    $options[$event] = new CJavaScriptExpression($this->$event);
            }
        }
        $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
        $assets_url = Yii::app()->assetManager->publish($assets, false, -1, YII_DEBUG ? true : null);
        Yii::app()->clientScript
                ->registerScriptFile($assets_url . '/lineItem.js', CClientScript::POS_END)
                ->registerScript('LineItem-' . md5(microtime()), "MyLineItem.init(" . CJavaScript::encode($options) . ")");
    }

}

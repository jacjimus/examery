<?php

/**
 * This is the model class for table "person".
 *
 * The followings are the available columns in table 'person':
 * @property string $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $gender
 * @property string $birthdate
 * @property integer $birthdate_estimated
 * @property string $profile_image
 * @property string $date_created
 * @property string $created_by
 * @property string $last_modified
 *
 * The followings are the available model relations:
 * @property PersonAddress[] $personAddresses
 */
class Person extends ActiveRecord {

        
        const BASE_DIR = 'profile';

        /**
         * Holds the temp file for profile image if file upload via ajax.
         * @var type
         */
        public $temp_profile_image;

       
        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_admins';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {
                return array(
                   
                    array('profile_image', 'length', 'max' => 128),
                   
                   array('temp_profile_image', 'safe'),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                   
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
                return array(
                    'id' => Lang::t('ID'),
                    
                    'profile_image' => Lang::t('Profile Image'),
                    
                );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return Person the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        public function beforeSave()
        {
                if (!empty($this->id))
                        $this->setProfileImage();
                return parent::beforeSave();
        }

        public function afterDelete()
        {
                $dir = $this->getBaseDir() . DS . $this->id;
                if (is_dir($dir))
                        Common::deleteDir($dir);

                return parent::afterDelete();
        }

        /**
         * Get the dir of a user
         * @param string $id
         */
        public function getDir($id = null)
        {
                if (!empty($this->id))
                        $id = $this->id;
                return Common::createDir($this->getBaseDir() . DS . $id);
        }

        public function getBaseDir()
        {
                return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
        }

        /**
         * Gets user profile image path
         * @param type $id
         * @return string
         */
        public function getProfileImagePath($id = null)
        {
                if (!empty($this->id))
                        $id = $this->id;
                $profile_image = !empty($this->id) ? $this->profile_image : $this->get($id, 'profile_image');
                if (!empty($profile_image)) {
                        $file_path = $this->getDir($id) . DS . $profile_image;

                        if (file_exists($file_path))
                                return $file_path;
                }
                return $this->getDefaultProfileImagePath();
        }

        protected function getDefaultProfileImagePath()
        {
                return Yii::getPathOfAlias('webroot.themes') . DS . Yii::app()->theme->name . DS . 'images' . DS . 'default-profile-image.png';
        }

        public function setProfileImage()
        {
                //using fineuploader
                if (!empty($this->temp_profile_image)) {
                        $temp_file = Common::parseFilePath($this->temp_profile_image);
                        $file_name = $temp_file['name'];
                        $temp_dir = $temp_file['dir'];
                        if (copy($this->temp_profile_image, $this->getDir() . DS . $file_name)) {
                                if (!empty($temp_dir))
                                        Common::deleteDir($temp_dir);
                                $this->profile_image = $file_name;
                                $this->temp_profile_image = null;
                        }
                }
        }

}

<?php

/**
 * This is the model class for table "modules_enabled".
 *
 * The followings are the available columns in table 'modules_enabled':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $status
 * @property string $date_created
 * @property integer $created_by
 */
class ModulesEnabled  {

        const STATUS_ENABLED = 'Enabled';
        const STATUS_DISABLED = 'Disabled';
        //module constants
        const MOD_ACCESS_CODES = 'accesscodes';
        const MOD_EXAMS = 'exams';
        const MOD_LOGS = 'logs';
        const MOD_REPORTS = 'reports';
        const MOD_USERS = 'users';
        const MOD_SETUP = 'setup';
        const MOD_FRONT = 'front';
      

        /**
         * @return string the associated database table name
         */
       public static function modules()
       {
           return array(
           self::MOD_ACCESS_CODES => self::STATUS_ENABLED,
           self::MOD_EXAMS => self::STATUS_ENABLED,
           self::MOD_LOGS => self::STATUS_ENABLED,
           self::MOD_REPORTS => self::STATUS_ENABLED,
           self::MOD_USERS => self::STATUS_ENABLED,
           self::MOD_SETUP => self::STATUS_ENABLED,
           self::MOD_FRONT => self::STATUS_ENABLED,
           );
       }
}

<div class="sidebar sidebar-fixed<?php echo $this->sidebar_collapsed ? ' menu-min' : '' ?>" id="sidebar">
        <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                        <a class="btn btn-success" href="<?php echo Yii::app()->createUrl('accesscodes/default/index') ?>" title="Manage Access codes"><i class="icon-archive"></i></a>
                        <a class="btn btn-warning" href="<?php echo Yii::app()->createUrl('accesscodes/orders/index') ?>" title="Manage orders"><i class="icon-ticket"></i></a>
                        <a class="btn btn-info" href="<?php echo Yii::app()->createUrl('exams/default/index') ?>" title="Manage Exams"><i class="icon-briefcase"></i></a>
                        <a class="btn btn-danger" href="<?php echo Yii::app()->createUrl('users/subjects/index') ?>" title="Manage Subjects"><i class="icon-tasks"></i></a>
                        <a class="btn btn-inverse" href="<?php echo Yii::app()->createUrl('reports/default/index') ?>" title="View Reports"><i class="icon-barcode"></i></a>
                        <a class="btn btn-danger" href="<?php echo Yii::app()->createUrl('settings/default/index') ?>" title="Settings"><i class="icon-wrench"></i></a>
                </div>
                <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                        <span class="btn btn-success"></span>
                        <span class="btn btn-info"></span>
                        <span class="btn btn-warning"></span>
                        <span class="btn btn-danger"></span>
                </div>
        </div><!-- #sidebar-shortcuts -->
        <ul class="nav nav-list my-nav">
            <li class="<?php echo $this->getModuleName() === 'admin' ? 'active open' : '' ?>"><a href="<?php echo Yii::app()->createUrl('admin/default/index') ?>" class=" dropdown-toggle">
                        <i class="icon-dashboard"></i>
                        <span class="menu-text"> <?php echo Lang::t('Dashboard') ?> </span>
                    <b class="arrow icon-angle-down"></b>
                    </a>
                     <ul class="submenu">
                       <li class="<?php echo $this->activeMenu === AdminModuleController::MENU_DASHBOARD ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('admin/default/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Home') ?></a></li>
                       </ul>  
                     <ul class="submenu">
                       <li class="<?php echo $this->activeMenu === AdminModuleController::MENU_PROFILE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('admin/default/profile') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Profile details') ?></a></li>
                       </ul>  
                </li>
                <!--ACCESS CODES MANAGER MODULE-->
                
                 <?php
                 
                if (Controller::isModuleEnabled(ModulesEnabled::MOD_ACCESS_CODES)):
                        $this->renderPartial(ModulesEnabled::MOD_ACCESS_CODES . '.views.layouts._sideLinks');
                endif;
                ?>
                <!--EXAMS / CONTENT MODULE-->
                 <?php
                if (Controller::isModuleEnabled(ModulesEnabled::MOD_EXAMS)):
                        $this->renderPartial(ModulesEnabled::MOD_EXAMS . '.views.layouts._sideLinks');
                endif;
                ?>
                
               
                <!--REPORTS MODULE-->
               <?php
                if (Controller::isModuleEnabled(ModulesEnabled::MOD_REPORTS)):
                        $this->renderPartial(ModulesEnabled::MOD_REPORTS . '.views.layouts._sideLinks');
                endif;
                ?>
                
                <!--SYSTEM LOGS MODULE-->
                 <?php
                 
                if (Controller::isModuleEnabled(ModulesEnabled::MOD_USERS)):
                        $this->renderPartial(ModulesEnabled::MOD_USERS . '.views.layouts._sideLinks');
                endif;
                
                 if ($this->showLink(UserResources::RES_USER_ACL)):
                        $this->renderPartial('users.views.layouts._sideLinks');
                endif;
                ?>
                <?php //$this->renderPartial('settings.views.layouts._sideLinks'); ?>
        </ul><!-- /.nav-list -->

        <div class="sidebar-collapse" id="sidebar-collapse">
                <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
        </div>
</div>
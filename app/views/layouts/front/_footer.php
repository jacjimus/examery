 <div class="footer">
      <div class="container">
        <div class="row">
          <!-- BEGIN COPYRIGHT -->
          <div class="col-md-10 col-sm-10padding-top-10">
              <a href="#">Terms of Service</a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<a href="#">Privacy Policy</a>&nbsp;&nbsp; | &nbsp;&nbsp;2014 © Examery. All Rights Reserved. 
              | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <?php
                  echo CHtml::link( 'Administration', Yii::app()->createUrl('auth/default/login') , array('target' => '_blank'))
                  ?>
            
          </div>
        
        </div>
      </div>
    </div>

<script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();    
            Layout.initOWL();
            Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
            Layout.initNavScrolling();
        });
    </script>
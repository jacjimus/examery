<head>
  <meta charset="utf-8">
   <title><?php echo $this->pageTitle; ?></title>
       
<link rel="shortcut icon" href="<?php echo $this->getPackageBaseUrl() ?>/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo $this->getPackageBaseUrl() ?>/images/favicon.ico" type="image/x-icon">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Exemery description" name="description">
  <meta content="Exemery keywords" name="keywords">
  <meta content="Athias Avians" name="author">

  <meta property="og:site_name" content="-CUSTOMER VALUE-">
  <meta property="og:title" content="-CUSTOMER VALUE-">
  <meta property="og:description" content="-CUSTOMER VALUE-">
  <meta property="og:type" content="website">
  <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
  <meta property="og:url" content="-CUSTOMER VALUE-">

  <link rel="shortcut icon" href="favicon.ico">

  <!-- Fonts START -->
<!--  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
   Fonts END -->

   <!--//load jquery from Google servers.-->
      
   
   
        <?php if (!YII_DEBUG): ?><script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script><?php endif; ?>
        
        
        <!--//fall back to a local jquery file if Google servers are unreachable-->
        <script>!window.jQuery && document.write('<script src="<?php echo $this->getPackageBaseUrl() . '/js/jquery-1.10.2.min.js' ?>"><\/script>')</script>
        <script type="text/javascript">
                if ("ontouchend" in document) {
                        document.write('<script src="<?php echo $this->getPackageBaseUrl() . '/js/jquery.mobile.custom.min.js' ?>"><\/script>');
                }
                
                
        </script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="<?php echo $this->getPackageBaseUrl() . '/js/html5shiv.js' ?>"></script>
        <script src="<?php echo $this->getPackageBaseUrl() . '/js/respond.min.js' ?>"></script>
        <![endif]-->
  
</head>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<?php $this->renderPartial('application.views.layouts.front._head') ?>
<body class="corporate">

    <!-- BEGIN HEADER -->
    <div class="header">
      <div class="container">
          <a class="site-logo" href="">
              <?php
             echo CHtml::image($this->getPackageBaseUrl() ."/images/logo.png", 'Examery', array('width' => "100%" , 'height' => "100%" ))
              
              ?>
          </a>

        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation pull-right font-transform-inherit">
          <ul>
            <li>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-20">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-md-12 margin-bottom-15">
                            <button class="btn btn-lg btn-block blue" type="button">Get Sample Papers</button>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-20">
                    <form role="form">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-md-12 margin-bottom-15">
                                <input type="text" class="form-control input-lg" placeholder="Quick Search">
                            </div>
                        </div>
                    </form>
                </div>
            </li>
            <!-- BEGIN TOP SEARCH -->
            <li class="menu-search">
              <span class="sep"></span>
              <i class="fa fa-search search-btn"></i>
              <div class="search-box">
                <form action="#">
                  <div class="input-group">
                    <input type="text" placeholder="Search" class="form-control">
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit">Search</button>
                    </span>
                  </div>
                </form>
              </div> 
            </li>
            <!-- END TOP SEARCH -->
          </ul>
        </div>
        <!-- END NAVIGATION -->
      </div>
    </div>
    <?php $this->renderPartial('application.views.widgets._alert') ?>
                       
    <?php echo $content ?>
    
    
  <?php $this->renderPartial('application.views.layouts.front._footer') ?> 
</body>

</html>
<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class FController extends CController {

        const GET_PARAM_RETURN_URL = 'r_url';

        /**
         * @var string the default layout for the controller view. Defaults to '//layouts/column1',
         * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
         */
        public $layout = '//layouts/front';

        /**
         * @var array context menu items. This property will be assigned to {@link CMenu::items}.
         */
        public $menu = array();

        /**
         * @var array the breadcrumbs of the current page. The value of this property will
         * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
         * for more details on how to specify this property.
         */
        public $breadcrumbs = array();

        /**
         * Stores commonly used system settings
         * @uses {@link CmsSettings}
         * @var array
         */
        public $settings = array();

        /**
         *
         * @var type
         */
        public $activeTab = 1;

        /**
         *
         * @var type
         */
        public $activeMenu = 1;
        /**
         *
         * @var type
         */
       public $package_id = 'ace';

        /**
         * The module's published assets base url
         * @var type
         */
        public $package_base_url;

        /**
         * The base title for a controller
         * @var type
         */
        public $resourceLabel;
        
        /**
         * The base title for a controller
         * @var type
         */
        
        public $resourceAddLabel;

        /**
         * Page description if any
         * @var type
         */
        public $pageDescription = '';

        /**
         * Used to control the display of page title
         * @var type
         */
        public $showPageTitle = TRUE;

        /**
         * Home module for the logged in user.
         * For admin users = "admin", for members=NULL
         * @var type
         */
        public $home_url;

        /**
         * Stores the user's privileges
         * @var type
         */
        public $privileges;

        /**
         *
         * @var type
         */
        public $resource;

        /**
         * Stores the cookie value of collapsed status of the sidebar
         * @var type
         */
        public $sidebar_collapsed = false;

       

        public function init()
        {
                parent::init();

                $this->registerPackage();
                $this->setSettings();

                
        }

        public function registerPackage()
        {
                $this->setPackage();

                $clientScript = Yii::app()->getClientScript();
                $this->package_base_url = $clientScript
                        ->addPackage($this->package_id, $this->package)
                        ->registerPackage($this->package_id)
                        ->getPackageBaseUrl($this->package_id);
        }

        public function getPackageBaseUrl()
        {
                return $this->package_base_url;
        }

        public function setPackage()
        {
                //register commonly used assets.
                $this->package = array(
                    'baseUrl' => Yii::app()->theme->baseUrl,
                    'js' => array(
                        'js-front/jquery/jquery.min.js',
                        'js-front/jquery/jquery-migrate.min.js',
                        'js-front/bootstrap/bootstrap.min.js',
                        'js-front/assets/frontend/layout/scripts/back-to-top.js',
                        'js-front/plugins/fancybox/fancybox.pack.js',
                        'js-front/plugins/carousel/owl.carousel.min.js',
                        'js-front/assets/frontend/layout/scripts/layout.js',
                        
                        
                    ),
                    'css' => array(
                        'css-front/font-awesome/css/font-awesome.min.css',
                        'css-front/bootstrap/css/bootstrap.min.css',
                        'css-front/plugins/fancybox/fancybox.css', //remove if using google fonts
                        'css-front/plugins/carousel/owl.carousel.css',
                        'css-front/assets/global/components.css',
                        'css-front/assets/frontend/layout/css/style.css',
                        'css-front/assets/frontend/layout/css/style-responsive.css',
                        'css-front/assets/frontend/layout/css/themes/red.css',
                        'css-front/assets/frontend/layout/css/custom.css',
                        
                    )
                );
        }

        public function setSettings()
        {
                if (YII_DEBUG)
                        Yii::app()->settings->deleteCache(Constants::CATEGORY_GENERAL);

                //get some commonly used settings
                $this->settings = Yii::app()->settings->get(Constants::CATEGORY_GENERAL, array(
                    Constants::KEY_COMPANY_NAME => 'Examery',
                    Constants::KEY_SITE_NAME => Yii::app()->name,
                    Constants::KEY_CURRENCY => 'KES',
                    Constants::KEY_PAGINATION => 20,
                ));
        }

      
        public function hasPrivilege($action = NULL)
        {
                if (NULL === $action)
                        $action = Acl::ACTION_VIEW;
                Acl::hasPrivilege($this->privileges, $this->resource, $action);
        }

        /**
         * Check if module is enabled
         * @param type $module_id
         * @return type
         */
        public static function isModuleEnabled($module_id)
        {
                
            return ModulesEnabled::modules($module_id);
        }

        /**
         * Get return link
         * @param type $url
         * @return type
         */
        public static function getReturnUrl($url = NULL)
        {
                $rl = filter_input(INPUT_GET, self::GET_PARAM_RETURN_URL);
                return !empty($rl) ? $rl : $url;
        }

}

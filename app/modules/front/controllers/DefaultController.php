<?php

/**
 *
 * @author James Makau <jacjimus@gmail.com>
 */
class DefaultController extends FrontModuleController {

        public function init()
        {
            
                parent::init();
        }
        
        public function actions()
    {
        return array(
            
        );
    }

        public function actionIndex()
        {
               $this->pageTitle = 'Index | Examery';

                
                $this->render('index', array(
                'featured' => Yii::app()->db->createCommand('SELECT C.title, C.id, C.description, S.logo_path FROM tbl_content C JOIN tbl_content_sources S ON C.source_id = S.id WHERE publish = "1" AND C.status = "Active" ORDER BY publish_order ASC')->queryAll()
                ));
        }

        
}

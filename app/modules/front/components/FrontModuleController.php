<?php

/**
 * Base controller for the auth module
 * @author James Makau <jacjimus@gmail.com>
 */
class FrontModuleController extends FController {

        public $layout = '//layouts/front';

        public function init()
        {
                parent::init();
        }

}

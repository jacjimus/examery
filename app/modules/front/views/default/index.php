

<div class="page-slider margin-bottom-40">
        <div class="fullwidthbanner-container revolution-slider">
            <!-- Carousel
            ================================================== -->
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                
                  <div class="carousel-inner" style>
                      <div class="item active">
                          <img class="img-responsive" src="<?php echo $this->getPackageBaseUrl() ?>/css-front/assets/frontend/pages/img/revolutionslider/students.jpg" alt="First slide">
                      </div>
                      <div class="item">
                          <img class="img-responsive" src="<?php echo $this->getPackageBaseUrl() ?>/css-front/assets/frontend/pages/img/revolutionslider/students.jpg" alt="Second slide">
                      </div>
                      <div class="item">
                          <img class="img-responsive" src="<?php echo $this->getPackageBaseUrl() ?>/css-front/assets/frontend/pages/img/revolutionslider/students.jpg" alt="Third slide">
                      </div>
                  </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
            <!-- /.carousel -->
        </div>
    </div>
    <!-- END SLIDER -->

    
    <!-- BEGIN TOP CONTENT AREA -->
    <div class="container-fluid top_content_area col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">
      <div class="row">
        <!-- BEGIN BOTTOM ABOUT BLOCK -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-content-wrapper">
            <div class="row" style="min-height: 170px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top_content_area_upper_section">
                    <h1>Largest Form Four Exam Library</h1>
                    <h3>Preparing for your KCSE made easier</h3>
                    <p>
                        Join thousands of Form four pupils using Examery to access examinations
                        from high profile schools , Counties and Kenya National Examination Council (KNEC) 
                    </p>
                </div>
            </div>
            <div class="row margin-bottom-0">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 bottom-buttons">
                    <button class="btn btn-lg btn-block btn-left" type="button">BUY ONLINE</button>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 bottom-buttons">
                    <button class="btn btn-lg btn-block btn-right" type="button">USE SCRATCH CARD</button>
                </div>
            </div>
        </div>
        <!-- END BOTTOM ABOUT BLOCK -->       
      </div>
    </div>
    <!-- END BOTTOM CONTENT AREA -->
   
    
    <!-- BEGIN GREY AREA -->
    <div class="container-fluid dark_ribbon">
      <div class="row">
        <!-- BEGIN BOTTOM ABOUT BLOCK -->
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 dark_ribbon_top_content dark_ribbon_top_content_left">
            <div class="row">
                <div class="col-lg-12 col-centered">
                    <i class="fa fa-weixin"></i>&nbsp;&nbsp;&nbsp;SAMPLE EXAMS
                </div>
            </div> 
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 dark_ribbon_top_content dark_ribbon_top_content_left">
            <div class="row">
                <div class="col-lg-12 col-centered">
                    <i class="fa fa-weixin"></i>&nbsp;&nbsp;&nbsp;BUY ACCESS CODES
                </div>
            </div> 
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 dark_ribbon_top_content">
            <div class="row">
                <div class="col-lg-12 col-centered">
                    <i class="fa fa-lock"></i>&nbsp;&nbsp;&nbsp;ACCESS YOUR EXAMS
                </div>
            </div> 
        </div>
        <!-- END BOTTOM ABOUT BLOCK -->       
      </div>
    </div>
    <!-- END GREY AREA -->
    
    <!-- BEGIN GREY AREA -->
    <div class="container-fluid grey_bars grey_bars_top">
      <div class="row">
        <!-- BEGIN BOTTOM ABOUT BLOCK -->
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 grey_bar_top_content grey_bar_top_content_left">
            <div class="row">
                <div class="col-lg-12 col-centered">
                    <i class="fa fa-weixin"></i>&nbsp;&nbsp;&nbsp;SATISFACTION GUARANTEED
                </div>
            </div> 
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 grey_bar_top_content">
            <div class="row">
                <div class="col-lg-12 col-centered">
                    <i class="fa fa-lock"></i>&nbsp;&nbsp;&nbsp;SECURE CHECKOUT
                </div>
            </div> 
        </div>
        <!-- END BOTTOM ABOUT BLOCK -->       
      </div>
    </div>
    <!-- END GREY AREA -->
    
    <div class="main" style="background: #e6e6e6;">
      <hr style="margin-top: 0px;">
      <div class="" style="min-height: 50px !important; height: auto;">
        <!-- BEGIN CLIENTS -->
        <div class="row margin-bottom-20 our-clients margin-top-10">
            <div class="col-md-2" style="margin-left: 8%;">
              <h2><a href="#"><?php echo Lang::t('Featured Exams') ?></a></h2>
            <p>Major & New Exams</p>
          </div>
          <div class="col-md-9">
            <div class="owl-carousel owl-carousel6-brands" style="margin-top: -5px !important;">
                
                <?php
                //var_dump($featured['title']);
                foreach ($featured As $f):
                    $logo = getSourceLogo($f['logo_path']);
                    echo '<div class="client-item client-item-border" style="border-left: 1px solid #D0D0D0;">';
                echo '<span  class="img-res+ponsive" >';
                echo '<h4>'.$f['title'] .'</h4>';
                echo '<a href="#">';
               echo $f['description'] ;
                //echo CHtml::image($logo, $f['title'], array("class"=>"img-responsive" , 'width' => 100 , 'height' => 100))
                ?>
                <?php
                   echo '</a>';
                  echo '</span>'
                    . '</div>'; 
                  endforeach;
                ?>
                              
            </div>
          </div>          
        </div><div class="container-fluid grey_bars">
      <div class="row">
        <!-- BEGIN BOTTOM ABOUT BLOCK -->
        <div class="col-md-4 col-sm-6 pre-footer-col">

        </div>
        <!-- END BOTTOM ABOUT BLOCK -->       
      </div>
    </div>

    
    <?php
    
    function getSourceLogo($image)
    {
        if(!empty($image))
        {
        
        return Yii::app()->baseUrl.'/public/sources/'.$image;
        }
     
     return $img = Yii::app()->baseUrl.'/public/sources/default_featured_exam.jpg';
 
    }           
    
   
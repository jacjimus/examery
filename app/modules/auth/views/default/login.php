<div class="login-container">
    
    <div class="space-12"></div>
    <div class="position-relative">
        <div id="login-box" class="login-box visible widget-box no-border">
            <div class="widget-body">
                <div class="widget-main">
                   
                    <h4 class="header blue lighter bigger">
                        
                        <div class="center">
                            <h3><span class="green"><?php echo $this->settings[Constants::KEY_SITE_NAME] ?></span></h3>
                        </div>
                    </h4>
                    <div class="space-6"></div>
                    <?php $this->renderPartial('application.views.widgets._alert') ?>
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'login-form',
                        'enableClientValidation' => false,
                        'focus' => array($model, 'username'),
                        'clientOptions' => array(
                            'validateOnSubmit' => false,
                        ),
                        'htmlOptions' => array(
                            'class' => '',
                        )
                    ));
                    ?>
                    <?php echo $form->errorSummary($model, ''); ?>
                    <fieldset>
                        <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => $model->getAttributeLabel('username'))); ?>
                                <i class="icon-user"></i>
                            </span>
                        </label>
                        <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                                <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => $model->getAttributeLabel('password'))); ?>
                                <i class="icon-lock"></i>
                            </span>
                        </label>
                        <div class="clearfix"> <a href="<?php echo $this->createUrl('forgotPassword') ?>" class="forgot-box">I forgot my password</a></div>
                        <div class="space"></div>
                            
                             <section>
                    <div class="text-left" style="padding-left: 0px;">
                        <?php if (CCaptcha::checkRequirements()): ?>
                            <?php
                            $this->widget('CCaptcha', array(
                                'buttonLabel' => '<img src="' .Yii::app()->request->baseUrl  .'/themes/default/images/refresh.png"   align="right" title="New Code"/>',
                                'imageOptions' => array('style' => 'width:250px;'),
                            ));
                            ?>
                            <br/>
                            <?php echo CHtml::activeTextField($model, 'verifyCode', array('style' => 'width:300px; padding : 5px;')); ?>
                            <p class="help-block"><?php echo Lang::t('Enter the code above.') ?></p>
                        <?php endif; ?>
                    </div>
                </section>
                        
                        <div class="clearfix">
                           <div class="clearfix"> <a href="<?php echo $this->createUrl('') ?>" class="forgot-box">Visit the website</a></div>
                        <button type="submit" class="width-35 pull-right btn btn-sm btn-primary"><i class="icon-key"></i>Login</button>
                        </div>
                        <div class="space-4"></div>
                    </fieldset>
                    <?php 
                    
                    $this->endWidget(); ?>
                </div><!-- /widget-main -->
                <div class="toolbar clearfix">
                   
                </div>
            </div><!-- /widget-body -->
        </div><!-- /login-box -->
    </div><!-- /position-relative -->
</div>
<?php

<!DOCTYPE html>
<html lang="en">
        <?php $this->renderPartial('auth.views.layouts._head') ?>
        <body class="login-layout">
                <div class="main-container">
                        <div class="main-content">
                                <div class="row">
                                        <div class="col-sm-10 col-sm-offset-1">
                                                <?php echo $content; ?>
                                        </div><!--/.col-->
                                </div><!--/.row-->
                        </div>
                </div><!--/.main-container-->
                <div class="modal fade" id="my_bs_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div>
        </body>
</html>


<?php

/**
 * Home controller
 * @author James Makau <jacjimus@gmail.com>
 */
class DefaultController extends AdminModuleController {

    public function init() {
        parent::init();
        $this->activeMenu = self::MENU_DASHBOARD;
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'profile', 'chart', 'monthly_term', 'shortcode', 'subs', 'allsubs'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex($id = NULL, $action = NULL) {
        // var_dump($this->addToAllSubsSets());
        $this->pageTitle = 'Dashboard';
        $this->render('dashboard', array(
            'model' => new Syncorder(),
        ));
    }

    public function actionMonthly_term() {
        Yii::app()->fusioncharts->setChartOptions(array('caption' => 'Monthly Terminations', 'xAxisName' => 'Date', 'yAxisName' => 'No of Terminations', 'labelDisplay' => 'ROTATE', 'slantLabels' => '1', 'showValues' => 0));


        $this->getDays();

        $this->getDataSets();
        $this->addToDataSets();

        Yii::app()->fusioncharts->useI18N = true;

        Yii::app()->fusioncharts->getXMLData(true);
    }

    public function actionShortcode() {
        Yii::app()->fusioncharts->setChartOptions(array('caption' => 'Shortcodes Terminations for  ' . date('M - Y'), 'xAxisName' => 'Short code', 'yAxisName' => 'No of Terminations', 'labelDisplay' => 'ROTATE', 'slantLabels' => '1', 'showValues' => 0));

        $this->getCodes();
        $options = array();
        
        Yii::app()->fusioncharts->addDataSet('data_unique_key', $options);

        $this->addToShortCodeSets();
        Yii::app()->fusioncharts->useI18N = true;

        Yii::app()->fusioncharts->getXMLData(true);
    }

    public function actionSubs() {
        Yii::app()->fusioncharts->setChartOptions(array('caption' => 'Total Shortcodes Subscribers ', 'xAxisName' => 'Short code', 'yAxisName' => 'No of sub', 'labelDisplay' => 'ROTATE', 'slantLabels' => '1', 'showValues' => 0));


        $this->getCodes();
        $options = array("seriesname" => "Subscriptions", "color" => "81C653");
        Yii::app()->fusioncharts->addDataSet('data_unique_key1', $options);
        $options = array("seriesname" => "Un-subscriptions", "color" => "");
        Yii::app()->fusioncharts->addDataSet('data_unique_key2', $options);

        $this->addToSubsSets();
        Yii::app()->fusioncharts->useI18N = true;

        Yii::app()->fusioncharts->getXMLData(true);
    }

    public function actionAllsubs() {
        Yii::app()->fusioncharts->setChartOptions(array('caption' => 'All subscribers per shortcode ', 'xAxisName' => 'Short code', 'yAxisName' => 'No of subs', 'labelDisplay' => 'ROTATE', 'slantLabels' => '1', 'showValues' => 0));

        $this->getsubsCodes();
        
        //$options = array();
       // $colors = array('32de12', '00ff00', 'cd5c5c', 'ffa500', 'aaffee', '3423cd', '000000', 'ff44de');
        $options = array( "color" => 'aaffee');
        Yii::app()->fusioncharts->addDataSet('data_unique_key', $options);

        $this->addToAllSubsSets();
        Yii::app()->fusioncharts->useI18N = true;

        Yii::app()->fusioncharts->getXMLData(true);
    }

    public function actionProfile($id = NULL, $action = NULL) {
        if (NULL === $id)
            $id = Yii::app()->user->id;
        $user_model = Users::model()->loadModel($id);
        $person_model = Person::model()->loadModel($id);

        //$this->resource = Users::USER_RESOURCE_PREFIX . $user_model->user_level;
        if (!Users::isMyAccount($id))
            $this->hasPrivilege();

        $this->pageTitle = $user_model->first_name;
        $this->showPageTitle = TRUE;

        

        if (!empty($action)) {
            if (!Users::isMyAccount($id)) {
                $this->checkPrivilege($user_model, Acl::ACTION_UPDATE);
            }
            switch ($action) {
                case Users::ACTION_UPDATE_PERSONAL:
                    $this->update($person_model);
                    break;
                case Users::ACTION_UPDATE_ACCOUNT:
                    $this->update($user_model);
                    break;
                
                case Users::ACTION_RESET_PASSWORD:
                    $this->resetPassword($user_model);
                    break;
                case Users::ACTION_CHANGE_PASSWORD:
                    $this->changePassword($user_model);
                    break;
                default :
                    $action = NULL;
            }
        }

        $this->render('view', array(
            'user_model' => $user_model,
            'person_model' => $person_model,
            'action' => $action
        ));
    }

    private function getDays() {

        $currentdays = intval(date("t"));
        $str = date('M-Y');
        $i = 0;
        $days = array();
        while ($i++ < $currentdays) {
            $days[] = Yii::app()->fusioncharts->addCategory(array('label' => $i . '-' . $str));
        }
        return $days;
    }

    private function getCodes() {

        $i = 1;
        $dataset = array();
        $sql = "SELECT DISTINCT short_code from sdp.sdp_services WHERE costing_type = 1 order by short_code";
        foreach (Yii::app()->db->createCommand($sql)->queryAll() as $row) {
            $dataset[] = Yii::app()->fusioncharts->addCategory(array('label' => $row['short_code']));
            $i++;
        }
        return $dataset;
    }

    private function getsubsCodes() {

        $i = 1;
        $dataset = array();
        
        $sql = "SELECT DISTINCT short_code from sdp.sdp_services WHERE costing_type = 1 order by short_code";
        foreach (Yii::app()->db->createCommand($sql)->queryAll() as $row) {
            $dataset[] = Yii::app()->fusioncharts->addCategory(array('label' => $row['short_code']));
            $i++;
        }
        return $dataset;
    }

    private function getDataSets($val = NULL) {
        $i = 1;
        $dataset = array();
        $colors = array('32de12', '00ff00', 'cd5c5c', 'ffa500', 'aaffee', '3423cd', '000000', 'ff44de');
        $sql = "SELECT DISTINCT short_code from sdp.sdp_services WHERE costing_type = 1 order by short_code";
        foreach (Yii::app()->db->createCommand($sql)->queryAll() as $row) {
            $options = array('seriesName' => $row['short_code'], "color" => $colors[$i]); // must specify series anme
            $dataset[] = Yii::app()->fusioncharts->addDataSet('data_unique_key' . $i, $options);
            $i++;
        }
        
        return $dataset;
    }

   

    private function addToDataSets($val = NULL) {
        $i = 1;
        $dataset = array();


        $sql = "SELECT DISTINCT short_code from sdp.sdp_services WHERE costing_type = 1 order by short_code ";

        foreach (Yii::app()->db->createCommand($sql)->queryAll() as $row) {
            $currentdays = intval(date("t"));
            $k = 0;
            $set = array();


            while ($k++ < $currentdays) {
                $short_code = $row['short_code'];
                $sqlw = "call sdp.daily_val('$k' , '$short_code')";

                foreach (Yii::app()->db->createCommand($sqlw)->queryAll() As $val) {
                    $num = $val['count'];
                }
                $num = isset($num) ? $num : 0;


                $options = array('value' => $num);
                //$set[] = 
                $dataset[] = Yii::app()->fusioncharts->addSetToDataSet('data_unique_key' . $i, $options);
                ;
                $num = 0;
            }

            $i++;
        }



        return $dataset;
    }

    private function addToShortCodeSets($val = NULL) {
        $i = 1;
        $dataset = array();


        $sql = "SELECT DISTINCT short_code from sdp.sdp_services WHERE costing_type = 1 order by short_code ";

        foreach (Yii::app()->db->createCommand($sql)->queryAll() as $row) {
            $currentdays = intval(date("t"));
            $k = 0;
            $set = array();

            $short_code = $row['short_code'];
            $sqlw = "call sdp.monthly_val('$short_code')";

            foreach (Yii::app()->db->createCommand($sqlw)->queryAll() As $val) {
                $num = $val['count'];
            }
            $num = isset($num) ? $num : 0;


            $options = array('value' => $num);
            //$set[] = 
            $dataset[] = Yii::app()->fusioncharts->addSetToDataSet('data_unique_key', $options);
            ;
            $num = 0;


            $i++;
        }



        return $dataset;
    }

    private function addToAllSubsSets($val = NULL) {
        $i = 1;
        $dataset = array();


        $sql = "SELECT DISTINCT short_code from sdp.sdp_services WHERE costing_type = 1 order by short_code ";

        foreach (Yii::app()->db->createCommand($sql)->queryAll() as $row) {
            $short_code = $row['short_code'];
            $table = "SELECT DISTINCT service_table from sdp.sdp_services WHERE short_code = '$short_code' ";
            $num = 0;
            foreach (Yii::app()->db->createCommand($table)->queryAll() as $t) {
                $tab = "'sdp. $t[service_table]'";
                if (Yii::app()->db->schema->getTable('sdp.' . $t['service_table']) !== null) {
                    $find = "SELECT count(T.id) as num FROM sdp." . $t['service_table'] . " T JOIN sdp.sdp_services S on  
                             T.service_id = S.service_id WHERE S.short_code = '$short_code' AND S.costing_type = 1";
                    foreach (Yii::app()->db->createCommand($find)->queryAll() as $n) {
                        $num += ($tab != "furahia_ringtones_6" || $tab != "furahia_ringtones_12") ? $n['num'] : 0;
                    }
                }
            }



            $options = array('value' => $num);
            $dataset[] = Yii::app()->fusioncharts->addSetToDataSet('data_unique_key', $options);
            ;

            $i++;
            ;
        }


        return $dataset;
    }

    private function addToSubsSets($val = NULL) {
        $i = 1;
        $dataset = array();


        $sql = "SELECT DISTINCT short_code from sdp.sdp_services WHERE costing_type = 1 order by short_code ";

        foreach (Yii::app()->db->createCommand($sql)->queryAll() as $row) {
            $currentdays = intval(date("t"));
            $k = 0;
            $set = array();

            $short_code = $row['short_code'];
            $sqlw = "call sdp.subs_monthly_in('$short_code', '1')";
            $sql_out = "call sdp.subs_monthly_in('$short_code' , '2')";

            foreach (Yii::app()->db->createCommand($sqlw)->queryAll() As $val) {
                $num1 = $val['subs'];
            }
            foreach (Yii::app()->db->createCommand($sql_out)->queryAll() As $val2) {
                $num2 = $val2['subs'];
            }
            $num1 = isset($num1) ? $num1 : 0;
            $num2 = isset($num2) ? $num2 : 0;


            $options1 = array('value' => $num1);
            $options2 = array('value' => $num2);
            //$set[] = 
            $dataset[] = Yii::app()->fusioncharts->addSetToDataSet('data_unique_key1', $options1);
            $dataset[] = Yii::app()->fusioncharts->addSetToDataSet('data_unique_key2', $options2);
            ;
            $num = 0;


            $i++;
        }



        return $dataset;
    }

}

<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class AdminModuleController extends Controller {
        //menu constants

        const MENU_DASHBOARD = 'DASHBOARD';
        const MENU_PROFILE = 'PROFILE';
        const MENU_HELP = 'HELP';

        public function init()
        {
                parent::init();
                
        }

}


<?php

/**
 * This is the model class for table "tbl_content_package".
 *
 * The followings are the available columns in table 'tbl_content_package':
 * @property integer $package_code
 * @property string $description
 * @property string $status
 * @property double $price_per_session
 *
 * The followings are the available model relations:
 * @property TblSubjects[] $tblSubjects
 */
class Packages extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    const STATUS_ACTIVE = 'Active';
    const STATUS_INACTIVE = 'Inactive';
    public function tableName() {
        return 'tbl_content_package';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('package_code, description', 'required'),
            array('package_code', 'unique'),
            array('description', 'length', 'max' => 50),
            array('status', 'length', 'max' => 25),
            array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }
   
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'package_code' => 'Package Code',
            'description' => 'Description',
            'status' => 'Status',
           
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Packages the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchParams() {
        return array(
            array('package_code', self::SEARCH_FIELD, true, 'OR'),
            array('description', self::SEARCH_FIELD, true, 'OR'),
           // array('buying_center', self::SEARCH_FIELD, true, 'OR'),
            
        );
    }

}

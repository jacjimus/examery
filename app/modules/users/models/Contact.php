<?php

/**
 * This is the model class for table "tbl_content_source_contact".
 *
 * The followings are the available columns in table 'tbl_content_source_contact':
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $timestamp
 *
 * The followings are the available model relations:
 * @property TblContentSources[] $tblContentSources
 */
class Contact extends ActiveRecord implements IMyActiveSearch
{
	/**
	 * @return string the associated database table name
	 */
    public $names;
	public function tableName()
	{
		return 'tbl_content_source_contact';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name ,email , phone', 'required'),
			array('name', 'length', 'max'=>100),
                       array('phone', 'numerical', 'integerOnly' => true),
			array('phone', 'length', 'max'=>25),
			array('email', 'email'),
			array('timestamp', 'safe'),
			array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblContentSources' => array(self::HAS_MANY, 'TblContentSources', 'contact_person_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'phone' => 'Phone',
			'email' => 'Email',
			'timestamp' => 'Timestamp',
		);
	}

        public function afterFind()
        {
            $this->names = $this->name.' '.$this->email;
            return parent::afterFind();
        }
        /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchParams() {
        return array(
           // array('package_code', self::SEARCH_FIELD, true, 'OR'),
            array('name', self::SEARCH_FIELD, true, 'OR'),
            array('email', self::SEARCH_FIELD, true, 'OR'),
            
        );
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

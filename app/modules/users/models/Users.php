<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $profile_image
 * @property string $password
 * @property string $salt
 * @property integer $role_id
 * @property string $date_joined
 * @property integer $created_by
 * @property string $last_login
 */
class Users extends ActiveRecord {

        const SCENARIO_CHANGE_PASSWORD = 'changePass'; //When a user's password is changed by the admin or a user recovers password (forgot password)
        const SCENARIO_RESET_PASSWORD = 'resetPass';
        const SCENARIO_ACTIVATE_ACCOUNT = 'activation';
        const SCENARIO_SIGNUP = 'sign_up';
        //status constants
        const STATUS_ACTIVE = 'Active';
        const STATUS_PENDING = 'Pending';
        const STATUS_BLOCKED = 'Blocked';
        //user resource prefix
        const USER_RESOURCE_PREFIX = 'USER_';
        const BASE_DIR = 'profile';

        //holder for dept_id
        public $client_id;
        public $name;
        public $role;
        

        /**
         * Holds the temp file for profile image if file upload via ajax.
         * @var type
         */
        public $temp_profile_image;

        /**
         *
         * @var type
         */
        public $send_email = false;

        /**
         * confirm password
         * @var type
         */
        public $confirm;

        /**
         * Current user password
         * Used for when a user wants to change his/her password
         * @var type
         */
        public $currentPassword;

        /**
         * temp buffer for new password during password reset action
         * @var type
         */
        public $pass;

        /**
         *
         * @var type
         */
        public $verifyCode;

        //profile actions constants
        const ACTION_UPDATE_PERSONAL = 'u_personal';
        const ACTION_UPDATE_ACCOUNT = 'u_account';
        const ACTION_UPDATE_ADDRESS = 'u_address';
        const ACTION_RESET_PASSWORD = 'reset_p';
        const ACTION_CHANGE_PASSWORD = 'change_p';

        public function behaviors()
        {
                $behaviors = array();
                $behaviors['UserBehavior'] = array(
                    'class' => 'application.modules.users.components.behaviors.UserBehavior',
                );
                return array_merge(parent::behaviors(), $behaviors);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_admins';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {
                return array(
                    array('username, email, password, salt, role_id, first_name , last_name', 'required'),
                     array('role_id, ', 'numerical', 'integerOnly' => true),
                    array('username', 'length', 'max' => 30),
                    array('email, password, salt', 'length', 'max' => 128),
                    array('status', 'length', 'max' => 15),
                    array('email', 'email', 'message' => Lang::t('Please enter a valid email address')),
                    array('username, email', 'unique', 'message' => Lang::t('{value} already exists')),
                    array('username,password', 'length', 'min' => 4, 'max' => 20, 'on' => ActiveRecord::SCENARIO_CREATE . ',' . self::SCENARIO_SIGNUP, 'message' => Lang::t('{attribute} length should be between {min} and {max}.')),
                    array('confirm', 'compare', 'compareAttribute' => 'password', 'on' => self::SCENARIO_CHANGE_PASSWORD . ',' . ActiveRecord::SCENARIO_CREATE . ',' . self::SCENARIO_RESET_PASSWORD . ',' . self::SCENARIO_SIGNUP, 'message' => Lang::t('Passwords do not match.')),
                    array('currentPassword', 'compare', 'compareAttribute' => 'pass', 'on' => self::SCENARIO_CHANGE_PASSWORD, 'message' => Lang::t('{attribute} is wrong')),
                    array('currentPassword', 'required', 'on' => self::SCENARIO_CHANGE_PASSWORD),
                    array('username', 'match', 'pattern' => '/^([a-zA-Z0-9_])+$/', 'message' => Lang::t('{attribute} can contain only alphanumeric characters and/or underscore(_).')),
                    array('send_email,confirm', 'safe'),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
                return array(
                    'id' => Lang::t('ID'),
                    'username' => Lang::t('Username'),
                    'email' => Lang::t('Email'),
                    'status' => Lang::t('Status'),
                    'password' => Lang::t('Password'),
                    'confirm' => Lang::t('Confirm Password'),
                    'date_joined' => Lang::t('Date Created'),
                    'last_login' => Lang::t('Last Login'),
                    'profile_image' => Lang::t('Pofile Image'),
                );
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         * @param string $className active record class name.
         * @return Users the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        

        public function afterFind()
        {
               
                $this->role = $this->getRole($this->role_id);
                return parent::afterFind();
        }

        public function beforeSave()
        {
            $this->date_joined = date('Y-m-d');
            $this->setProfileImage();
                return parent::beforeSave();
        }


        /**
         * Validate password for login
         * @param type $password
         * @return type
         */
        public function validatePassword($password)
        {
                return $this->salt . md5($password) !== $this->password;
        }

       

        /**
         * Get user acc status (mainly for displayinng in dropdown list)
         * @return type
         */
        public static function statusOptions()
        {
                return array(
                    self::STATUS_ACTIVE => Lang::t(self::STATUS_ACTIVE),
                    self::STATUS_PENDING => Lang::t(self::STATUS_PENDING),
                    self::STATUS_BLOCKED => Lang::t(self::STATUS_BLOCKED),
                );
        }
        
        /**
         * Get the dir of a user
         * @param string $id
         */
        public function getDir($id = null)
        {
                if (!empty($this->id))
                        $id = $this->id;
                return Common::createDir($this->getBaseDir() . DS . $id);
        }

        public function getBaseDir()
        {
                return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
        }

        public function getProfileImagePath($id = null)
        {
                if (!empty($this->id))
                        $id = $this->id;
                $exam_image = !empty($this->id) ? $this->profile_image : $this->get($id, 'profile_image');
                if (!empty($exam_image)) {
                        $file_path = $this->getDir($id) . DS . $exam_image;

                        if (file_exists($file_path))
                                return $file_path;
                }
                return $this->getDefaultProfileImagePath();
        }

        protected function getDefaultProfileImagePath()
        {
                return Yii::getPathOfAlias('webroot.themes') . DS . Yii::app()->theme->name . DS . 'images' . DS . 'default-profile-image.png';
        }

        public function setProfileImage()
        {
                //using fineuploader
           
                if (!empty($this->temp_profile_image)) {
                        $temp_file = Common::parseFilePath($this->temp_profile_image);
                        $file_name = $temp_file['name'];
                        $temp_dir = $temp_file['dir'];
                        if (copy($this->temp_profile_image, $this->getDir() . DS . $file_name)) {
                                if (!empty($temp_dir))
                                        Common::deleteDir($temp_dir);
                                $this->profile_image = $file_name;
                                
                                $this->temp_profile_image = null;
                        }
                }
        }

       

        /**
         * Whether the logged in user can update a given user
         * @param type $controller
         * @return type
         */
        public function canBeModified($controller, $type = Acl::ACTION_UPDATE)
        {
                 return true;
        }

        /**
         * Get dept id of a user
         * @param type $id
         * @return type
         */
       
        public function getRole($id)
        {
                if (empty($id))
                        return NULL;
                $dept_id = UserRoles::model()->getScaler('description', '`id`=:t1', array(':t1' => $id));
                return !empty($dept_id) ? $dept_id : NULL;
        }

        /**
         * Update user dept
         */
       
        /**
         * Check whether account belongs to a user
         * @param type $id
         * @return type
         */
        public static function isMyAccount($id)
        {
                return $id === Yii::app()->user->id;
        }

        /**
         * Update last login
         * @param type $id
         * @return type
         */
        public function updateLastLogin($id)
        {
                return Yii::app()->db->createCommand()
                                ->update($this->tableName(), array('last_login' => new CDbExpression('NOW()')), '`id`=:t1', array(':t1' => $id));
        }
        
        public function getFetchCondition()
        {
                $condition = '(`role_id`<>"")';
               
                

                return $condition;
        }

}

<?php

/**
 * This is the model class for table "tbl_account_buddles".
 *
 * The followings are the available columns in table 'tbl_account_buddles':
 * @property integer $id
 * @property string $acc_buddle
 * @property double $price
 * @property integer $subject_package_id
 * @property integer $sessions
 * @property integer $status
 * @property integer $subject
 */
class Buddles extends ActiveRecord implements IMyActiveSearch
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_account_buddles';
	}
        
        public  $subject;


        /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject_package_id, sessions , price', 'required'),
			array('sessions', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('acc_buddle , status', 'length', 'max'=>25),
			array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
        
        public function beforeSave() {
            $this->acc_buddle = number_format($this->price, 0).$this->subject_package_id.$this->sessions;
            return parent::beforeSave();
        }
        
        public function afterFind() {
             $this->subject = $this->getSubject($this->subject_package_id);
            return parent::afterFind();
        }
         public function getSubject($id)
        {
                if (empty($id))
                        return NULL;
                $subject = Packages::model()->getScaler('description', '`package_code`=:t1', array(':t1' => $id));
                return !empty($subject) ? $subject : NULL;
        }
        /**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'acc_buddle' => 'Acc Buddle',
			'price' => 'Price',
			'subject_package_id' => 'Subject Package',
			'subject' => 'Subject Package',
			'sessions' => 'Sessions',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchParams() {
        return array(
           // array('package_code', self::SEARCH_FIELD, true, 'OR'),
            array('acc_buddle', self::SEARCH_FIELD, true, 'OR'),
            array('sessions', self::SEARCH_FIELD, true, 'OR'),
            array('status', self::SEARCH_FIELD, true, 'OR'),
            array('price', self::SEARCH_FIELD, true, 'OR'),
            
        );
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Buddles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

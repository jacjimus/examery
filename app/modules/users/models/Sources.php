<?php

/**
 * This is the model class for table "tbl_content_sources".
 *
 * The followings are the available columns in table 'tbl_content_sources':
 * @property integer $id
 * @property string $description
 * @property string $temp_logo_path
 * @property string $status
 * @property integer $contact_person_id
 *
 * The followings are the available model relations:
 * @property TblContent[] $tblContents
 * @property TblContentSourceContact $contactPerson
 */
class Sources extends ActiveRecord implements IMyActiveSearch
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_content_sources';
	}
        
        const BASE_DIR = 'sources';
        
        public $temp_logo_path;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contact_person_id', 'numerical', 'integerOnly'=>true),
			array('description , temp_logo_path', 'length', 'max'=>255),
			array('status', 'length', 'max'=>25),
                      array('description,temp_logo_path', 'safe'),
			array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblContents' => array(self::HAS_MANY, 'TblContent', 'source_id'),
			'contactPerson' => array(self::BELONGS_TO, 'TblContentSourceContact', 'contact_person_id'),
		);
	}

        public function beforeSave()
        {
           $this->setProfileImage();
                      //var_dump($this->id);die;
                return parent::beforeSave();
        }
        public function getDir($id = null)
        {
                if (empty($this->id))
                        $id = $this->id;
                return Common::createDir($this->getBaseDir() . DS . $id);
        }

        public function getBaseDir()
        {
                return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
        }

        public function getProfileImagePath($id = null)
        {
                if (!empty($this->id))
                        $id = $this->id;
                $exam_image = !empty($this->id) ? $this->logo_path : $this->get($id, 'logo_path');
                if (!empty($exam_image)) {
                        $file_path = $this->getDir($id) . DS . $exam_image;

                        if (file_exists($file_path))
                                return $file_path;
                }
                return $this->getDefaultProfileImagePath();
        }

        protected function getDefaultProfileImagePath()
        {
                return Yii::getPathOfAlias('webroot.themes') . DS . Yii::app()->theme->name . DS . 'images' . DS . 'default_featured_exam.jpg';
        }

        public function setProfileImage()
        {
                //using fineuploader
           
                if (!empty($this->temp_logo_path)) {
                        $temp_file = Common::parseFilePath($this->temp_logo_path);
                        $file_name = $temp_file['name'];
                        $temp_dir = $temp_file['dir'];
                        if (copy($this->temp_logo_path, $this->getDir() . DS . $file_name)) {
                                if (!empty($temp_dir))
                                        Common::deleteDir($temp_dir);
                                $this->logo_path = $file_name;
                                
                                $this->temp_logo_path = null;
                        }
                }
        }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
			'temp_logo_path' => 'Logo',
			'status' => 'Status',
			'contact_person_id' => 'Contact Person',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchParams() {
        return array(
           // array('package_code', self::SEARCH_FIELD, true, 'OR'),
            array('description', self::SEARCH_FIELD, true, 'OR'),
            //array('buying_center', self::SEARCH_FIELD, true, 'OR'),
            
        );
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sources the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * The user/staff controller
 * @author James Makau <jacjimus@gmail.com>
 */
class SourcesController extends UsersModuleController {

        public function init()
        {
                $this->resource = UserResources::RES_SETUP_SOURCE;
                $this->activeMenu = self::MENU_SETUP_SOURCE;
                $this->resourceLabel = 'Exam Sources';
               $this->showPageTitle = false;
                parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete,changeStatus,changeLevel,deleteLog', // we only allow deletion via POST request
                    'ajaxOnly + resetPassword,loginLog,activityLog',
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                    array('allow',
                        'actions' => array('index', 'view', 'update', 'create', 'delete' , 'add'),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

        public function actionCreate()
        {
                $this->hasPrivilege(Acl::ACTION_CREATE);
                $this->pageTitle = Lang::t('Add ' . $this->resourceLabel);

                $model = new Sources();
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                       //var_dump($model->temp_logo_path);die;
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('index'));
                        }
                }

                $this->render('create', array(
                    'model' => $model,
                ));
        }
        
         public function actionAdd() {
        $model = new Contact();
        $model_class_name = get_class($model);
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->save()) {
                if (Yii::app()->request->isAjaxRequest) {
                    echo CJSON::encode(array(
                        'status' => 'success',
                        'div' => "Person added",
                        'options' => "<option value=' $model->id ' selected='selected'>$model->name</option>"
                    ));
                    exit;
                } else
                    $this->redirect($this->actionCreate());
            }
            else {

                //echo 'could not insert';
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array(
                'status' => 'failure',
                'div' => $this->renderPartial('_contact', array('model' => $model), true)));
            exit;
        } else
            $this->render('_contact', array('model' => $model,));
    }
        
      public function actionUpdate($id)
        {
                $this->hasPrivilege(Acl::ACTION_UPDATE);
                $this->pageTitle = Lang::t('Edit ' . $this->resourceLabel);

                $model = Sources::model()->loadModel($id);
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                       // var_dump($model->attributes);die;
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('index'));
                        }
                }

                $this->render('update', array(
                    'model' => $model,
                ));
        }
        
       public function actionDelete($id)
    {
        $this->showLink(Acl::ACTION_DELETE);
        Sources::model()->loadModel($id)->delete();
    }

        /**
         * Lists all models.
         */
        public function actionIndex()
        {
           //   
                $this->hasPrivilege(Acl::ACTION_VIEW);
                
                $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));

                
               $searchModel = Sources::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'description');
              
              //var_dump($searchModel);die;
                $this->render('view', array(
                    'model' => $searchModel,
                    'usr' => '',
                    
                ));
        }
        
        

}

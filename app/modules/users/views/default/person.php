<?php echo CHtml::errorSummary($model, '') ?>
<div class="form-group">
    <label class="col-md-2 control-label"><?php echo Lang::t('Name') ?><span class="required">*</span></label>
    <div class="col-md-3">
        <?php echo CHtml::activeTextField($model, 'first_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('first_name'))); ?>
    </div>
    <div class="col-md-3">
        <?php echo CHtml::activeTextField($model, 'last_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('last_name'))); ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'gender', array('class' => 'col-md-2 control-label')); ?>
    <div class="col-md-6" style="padding-top: 4px;">
        <?php echo CHtml::activeRadioButtonList($model, 'gender', PersonHelper::genderOptions(), array('separator' => '&nbsp;&nbsp;&nbsp;&nbsp;')); ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'birthdate', array('class' => 'col-md-2 control-label')); ?>
    <div class="col-md-4">
        <div class="row">
            <div class="col-sm-4">
                <?php echo CHtml::activeDropDownList($model, 'birthdate_month', PersonHelper::birthDateMonthOptions(), array('class' => 'form-control')); ?>
            </div>
            <div class="col-sm-4">
                <?php echo CHtml::activeDropDownList($model, 'birthdate_day', PersonHelper::birthDateDayOptions(), array('class' => 'form-control')); ?>
            </div>
            <div class="col-sm-4">
                <?php echo CHtml::activeDropDownList($model, 'birthdate_year', PersonHelper::birthDateYearOptions(), array('class' => 'form-control')); ?>
            </div>
        </div>
    </div>
</div>




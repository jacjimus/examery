<?php
if (!isset($label_size)):
        $label_size = 4;
endif;
if (!isset($input_size)):
        $input_size = 6;
endif;
$label_class = "col-md-{$label_size} control-label";
$input_class = "col-md-{$input_size}";
if (!$model->getIsNewRecord()) {
        $can_update = $this->showLink($this->resource, Acl::ACTION_UPDATE) || !Users::isMyAccount($model->id);
}
?>

<?php if ($model->getIsNewRecord() || $can_update) : ?>

       
        
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'role_id', array('class' => $label_class)); ?>
                <div class="<?php echo $input_class ?>">
                        
                        <?php echo CHtml::activeDropDownList($model, 'role_id', UserRoles::model()->getListData('id', 'description'), array('class' => 'form-control')); ?>
                </div>
        </div>
<?php endif; ?>
<div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'email', array('class' => $label_class)); ?>
        <div class="<?php echo $input_class ?>">
                <?php echo CHtml::activeEmailField($model, 'email', array('class' => 'form-control', 'maxlength' => 128, 'type' => 'email')); ?>
                <?php echo CHtml::error($model, 'email') ?>
        </div>
</div>
<?php if ($model->isNewRecord): ?>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'username', array('class' => $label_class)); ?>
                <div class="<?php echo $input_class ?>">
                        <?php echo CHtml::activeTextField($model, 'username', array('class' => 'form-control', 'maxlength' => 30)); ?>
                        <?php echo CHtml::error($model, 'username') ?>
                </div>
        </div>
<?php endif; ?>
<?php if ($model->isNewRecord): ?>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'password', array('class' => $label_class)); ?>
                <div class="<?php echo $input_class; ?>">
                        <?php echo CHtml::activePasswordField($model, 'password', array('class' => 'form-control')); ?>
                        <?php echo CHtml::error($model, 'password') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'confirm', array('class' => $label_class)); ?>
                <div class="<?php echo $input_class; ?>">
                        <?php echo CHtml::activePasswordField($model, 'confirm', array('class' => 'form-control')); ?>
                        <?php echo CHtml::error($model, 'confirm') ?>
                </div>
        </div>
<?php endif ?>

<?php if ($model->isNewRecord): ?>
        <div class="form-group">
                <div class="col-md-offset-<?php echo $label_size ?> <?php echo $input_class; ?>">
                        <label class="checkbox"><?php echo CHtml::activeCheckBox($model, 'send_email'); ?> <?php echo Lang::t('Email the login details to the user.') ?></label>
                </div>
        </div>
<?php endif; ?>

<?php //if ($this->showLink(UserResources::RES_USER_ACL)): ?>
        <li class="<?php echo $this->getModuleName() === 'users' ? 'active open' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('users/packages/index') ?>" class="dropdown-toggle">
                        <i class="icon-lock"></i>
                        <span class="menu-text"> <?php echo Lang::t('System setup') ?></span>
                        <b class="arrow icon-angle-down"></b>
                </a>
                <ul class="submenu">
                        <?php if ($this->showLink(UserResources::RES_SETUP_PACKAGE)): ?>
                                <li class="<?php echo $this->activeMenu === UsersModuleController::MENU_SETUP_PACKAGE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('users/packages/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Add Subject group') ?></a></li>
                        <?php endif; ?>
                        <?php if ($this->showLink(UserResources::RES_SETUP_SUBJECT)): ?>
                                <li class="<?php echo $this->activeMenu === UsersModuleController::MENU_SETUP_SUBJECT ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('users/subjects/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Add Subjects') ?></a></li>
                        <?php endif; ?>
                        <?php if ($this->showLink(UserResources::RES_SETUP_SOURCE)): ?>
                                <li class="<?php echo $this->activeMenu === UsersModuleController::MENU_SETUP_SOURCE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('users/sources/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Add Exam source') ?></a></li>
                        <?php endif; ?>
                        <?php if ($this->showLink(UserResources::RES_SETUP_BUDDLE)): ?>
                                <li class="<?php echo $this->activeMenu === UsersModuleController::MENU_SETUP_BUDDLE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('users/buddles/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Manage Buddles') ?></a></li>
                        <?php endif; ?>
                        <?php if ($this->showLink(UserResources::RES_USER_ADMIN)): ?>
                                <li class="<?php echo $this->activeMenu === UsersModuleController::MENU_USER_ADMIN ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('users/default/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Manage Users') ?></a></li>
                        <?php endif; ?>
                        
                        <?php //if ($this->showLink(UserResources::RES_USER_ROLES)): ?>
                                <li class="<?php echo $this->activeMenu === UsersModuleController::MENU_USER_ROLES ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('users/roles/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Manage Roles') ?></a></li>
                        <?php //endif; ?>
                        
                </ul>
        </li>
<?php //endif; ?>


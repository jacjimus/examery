<div class="list-group">
        <a href="<?php echo Yii::app()->createUrl('users/default/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_USERS ? ' active' : '' ?>"><?php echo Lang::t('Users') ?></a>
        
        
        <?php if ($this->showLink(UsersModuleConstants::RES_USER_PRIVILEGES)): ?>
                <a href="<?php echo Yii::app()->createUrl('users/privileges/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_PRIVILEGES ? ' active' : '' ?>"><?php echo Lang::t('Farmer Privileges') ?></a>
        <?php endif; ?>
        <?php if ($this->showLink(UsersModuleConstants::RES_USER_PRIVILEGES)): ?>
               <a href="<?php echo Yii::app()->createUrl('users/agroPrivileges/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_AGRO_PRIVILEGES ? ' active' : '' ?>"><?php echo Lang::t('Agro Dealer Privileges') ?></a>
        <?php endif; ?>
        <?php if ($this->showLink(UsersModuleConstants::RES_USER_PRIVILEGES)): ?>
                <a href="<?php echo Yii::app()->createUrl('users/voucherPrivileges/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_VOUCHER_PRIVILEGES ? ' active' : '' ?>"><?php echo Lang::t('Voucher Privileges') ?></a>
        <?php endif; ?>
        <?php if ($this->showLink(UsersModuleConstants::RES_USER_PRIVILEGES)): ?>
                <a href="<?php echo Yii::app()->createUrl('users/invoicePrivileges/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_INVOICE_PRIVILEGES ? ' active' : '' ?>"><?php echo Lang::t('Invoice Privileges') ?></a>
        <?php endif; ?>
            
        <?php if ($this->showLink(UsersModuleConstants::RES_USER_PRIVILEGES)): ?>
                <a href="<?php echo Yii::app()->createUrl('users/supplierPrivileges/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_SUPPLIER_PRIVILEGES ? ' active' : '' ?>"><?php echo Lang::t('Supplier Privileges') ?></a>
        <?php endif; ?>
        
        <?php if ($this->showLink(UsersModuleConstants::RES_USER_PRIVILEGES)): ?>
                <a href="<?php echo Yii::app()->createUrl('users/inputPrivileges/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_INPUT_PRIVILEGES ? ' active' : '' ?>"><?php echo Lang::t('Input Privileges') ?></a>
        <?php endif; ?>
        <?php if ($this->showLink(UsersModuleConstants::RES_USER_PRIVILEGES)): ?>
                <a href="<?php echo Yii::app()->createUrl('users/reportsPrivileges/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_REPORTS_PRIVILEGES ? ' active' : '' ?>"><?php echo Lang::t('Reports Privileges') ?></a>
        <?php endif; ?>
        <?php if ($this->showLink(UsersModuleConstants::RES_USER_PRIVILEGES)): ?>
                <a href="<?php echo Yii::app()->createUrl('users/resources/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_RESOURCES ? ' active' : '' ?>"><?php echo Lang::t('Setup Privileges') ?></a>
        <?php endif; ?>
        <?php if ($this->showLink(UsersModuleConstants::RES_USER_ROLES)): ?>
                <a href="<?php echo Yii::app()->createUrl('users/roles/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_ROLES ? ' active' : '' ?>"><?php echo Lang::t('Manage User Roles') ?></a>
        <?php endif; ?>
        
        <?php //if ($this->showLink(UsersModuleConstants::RES_USER_LEVELS)): ?>
        <!-- <a href="<?php echo Yii::app()->createUrl('users/userLevels/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_USER_LEVELS ? ' active' : '' ?>"><?php echo Lang::t('Manage User Levels') ?></a>-->
        <?php //endif; ?>
                
</div>

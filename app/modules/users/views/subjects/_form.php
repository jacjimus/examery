<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<?php echo $form->errorSummary($model) ?>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    
    
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'package_code', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'package_code', Packages::model()->getListData('package_code', 'description' , true , "description != 'ALL' AND status = '".Packages::STATUS_ACTIVE. "'"), array('class' => 'form-control')); ?>
                   
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'description', array('class' => 'form-control', 'maxlength' => 20)); ?>
        </div>
    </div>
    
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'status', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'status', array(Packages::STATUS_ACTIVE => 'Active' , Packages::STATUS_INACTIVE => 'Inactive'), array('class' => 'form-control')); ?>
                   
        </div>
    </div>
    
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
   </div>
<?php $this->endWidget(); ?>

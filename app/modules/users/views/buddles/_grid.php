<?php

$grid_id = 'buddles-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => $this->pageTitle,
    'titleIcon' => null,
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => true ,'modal' => true),
    'toolbarButtons' => array(),
   // 'rowCssClass' => '$data->status==="0"?"bg-danger":""',
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
           
           
            array(
                'name' => 'acc_buddle',
            ),
            array(
                'name' => 'subject',
            ),
            array(
                'name' => 'price',
            ),
            array(
                'name' => 'sessions',
            ),
           
            
            array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'CHtml::tag("span", array("class"=>$data->status=="Active"?"badge badge-success":"badge badge-danger"), $data->status)',
        ),
            
           
            array(
                'class' => 'ButtonColumn',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'template' => '{edit}{trash}',
                'buttons' => array(
                    'edit' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t('Generate'),
                        ),
                    ),
                    'trash' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showlink("' . $this->resource . '","' . Acl::ACTION_DELETE . '")?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                    
                )
            ),
        ),
    )
));
?>
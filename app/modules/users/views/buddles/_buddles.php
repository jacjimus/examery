<?php

$grid_id = 'buddles-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => $this->pageTitle,
    'titleIcon' => null,
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => true ,'modal' => true),
    'toolbarButtons' => array(),
   // 'rowCssClass' => '$data->status==="0"?"bg-danger":""',
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
           
           
            array(
                'name' => 'order_no',
            ),
            array(
                'name' => 'group_name',
            ),
            array(
                'name' => 'buying_center',
            ),
            array(
                'name' => 'price',
            ),
            array(
                'name' => 'sessions',
            ),
            array(
                'name' => 'no_of_cards',
            ),
            array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'CHtml::tag("span", array("class"=>$data->status=="Generated"?"badge badge-success":"badge badge-danger"), $data->status)',
        ),
            
           
            array(
                'class' => 'ButtonColumn',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'template' => '{generate}{view}',
                'buttons' => array(
                    'generate' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-gears fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->order_no))',
                        'visible' => '$data->status == "Pending" ? true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t('Generate'),
                        ),
                    ),
                    'view' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->order_no))',
                        'visible' => '$data->status == "Generated" ? true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t('View Access codes'),
                        ),
                    ),
                    
                )
            ),
        ),
    )
));
?>
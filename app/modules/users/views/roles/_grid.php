
<?php

$grid_id = 'roles-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Manage User Roles'),
    'titleIcon' => null,
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => true ,'modal' => true),
    'toolbarButtons' => array(),
   // 'rowCssClass' => '$data->status==="0"?"bg-danger":""',
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
           
           array(
            'name' => 'description',
            'type' => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->description),Yii::app()->controller->createUrl("view",array("id"=>$data->primaryKey)))',
        ),
            
           
            array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'CHtml::tag("span", array("class"=>$data->status=="Active"?"badge badge-success":"badge badge-danger"), $data->status)',
        ),
            
           
            array(
                'class' => 'ButtonColumn',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'template' => '{view}{edit}{trash}',
                'buttons' => array(
                    'view' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                        'options' => array(
                            'class' => 'blue',
                            'title' => Lang::t('View Details'),
                        ),
                    ),
                    'edit' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'options' => array(
                            'class' => 'green',
                            'title' => Lang::t('Edit exam details'),
                        ),
                    ),
                    'trash' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showlink("' . $this->resource . '","' . Acl::ACTION_DELETE . '")?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'red',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                    
                )
            ),
        ),
    )
));
?>
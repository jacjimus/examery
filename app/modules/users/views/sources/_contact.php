<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<?php echo $form->errorSummary($model) ?>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'col-md-4 control-label')); ?>
        <div class="col-md-8">
             <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control ')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'phone', array('class' => 'col-md-4 control-label')); ?>
        <div class="col-md-8">
            <?php echo CHtml::activeTextField($model, 'phone', array('class' => 'form-control', 'maxlength' => 20)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'email', array('class' => 'col-md-4 control-label')); ?>
        <div class="col-md-8">
            <?php echo CHtml::activeTextField($model, 'email', array('class' => 'form-control', 'maxlength' => 50)); ?>
        </div>
    </div>
   
    
    
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
    <button type="button" class="btn btn-default" data-dismiss="modal" onclick = "{$('#dialogAccount').dialog('close');}"><i class="fa fa-times"></i>  <?php echo Lang::t('Close') ?></button>
</div>
<?php $this->endWidget(); ?>

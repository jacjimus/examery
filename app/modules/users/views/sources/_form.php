<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<?php echo $form->errorSummary($model) ?>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'description', array('class' => 'form-control', 'maxlength' => 20)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'contact_person_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->dropDownList($model, 'contact_person_id', Contact::model()->getListData('id' , 'name' , true ), array('class' => 'col-md-6' , 'id' => 'contact_person_id')); ?>
            <?php if ($this->showLink($this->resource, Acl::ACTION_CREATE)): ?><a class="" href="#" style = 'cursor: pointer;' onclick = "{addAcc(); $('#dialogAccount').dialog('open');}"><i class="icon-plus-sign"></i> <?php echo Lang::t('Create Contact person') ?></a><?php endif; ?>
               
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'status', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'status', array(Packages::STATUS_ACTIVE => 'Active' , Packages::STATUS_INACTIVE => 'Inactive'), array('class' => 'form-control')); ?>
                   
        </div>
    </div>
     
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'logo_path', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $this->renderPartial('_logo_field' ,array('model' => $model))?>
        </div>
    </div>
    
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
   </div>
<?php $this->endWidget(); ?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogAccount',
    'options' => array(
        'title' => 'Create Contact Person',
        'autoOpen' => false,
        'modal' => false,
        'width' => 450,
        'height' => 400,
    ),
));
?>
<div class="divAcc"></div>

<?php $this->endWidget(); ?>
                       

<script type="text/javascript">
   

    function addAcc()
    {
<?php
echo CHtml::ajax(array(
    'url' => array("sources/add"),
    'data' => "js:$(this).serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#dialogAccount div.divAcc form').submit(addAcc);
                }
                else
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                    setTimeout(\"$('#dialogAccount').dialog('close') \",400);
                    $('#contact_person_id').append(data.options);
                }
 
            } ",
))
?>;
        return false;

    }
    
   
</script>
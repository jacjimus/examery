<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ViewerController
 *
 * @author Jmakau
 */
class ViewerController extends ExamsModuleController {
    
   
    public function init() {
        $this->resource = UserResources::RES_EXAMS;
        $this->activeMenu = self::MENU_EXAM_CREATE;

        $this->resourceLabel = 'Exam';

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('Index'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    
    public function actionIndex($filename)
    {
        return PUBLIC_DIR.DS.'exams'. DS . $filename;
    }
}

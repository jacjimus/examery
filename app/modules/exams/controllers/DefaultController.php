<?php

class DefaultController extends ExamsModuleController {

    public function init() {
        $this->resource = UserResources::RES_EXAMS;
        $this->activeMenu = self::MENU_EXAM_CREATE;

        $this->resourceLabel = 'Exam';

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('Index', 'view', 'exam', 'create', 'add', 'ver', 'update', 'index', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    

    public function actionIndex() {
        $this->hasPrivilege(Acl::ACTION_VIEW);
         $this->showPageTitle = false;
         $this->pageTitle = Lang::t('Add Exams');
        
        $this->render('index', array(
            'model' => Exams::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION] , 'date_created DESC'),
        ));
    }
    
    public function actionView($id) {
        $this->hasPrivilege(Acl::ACTION_VIEW);
         $this->showPageTitle = false;
         $this->pageTitle = Lang::t('View  Exams Details');
        
        $this->render('view', array(
            'model' => Exams::model()->model()->loadModel($id),
        ));
    }
    
    
    
    public function actionExam() {
       
        $this->render('pdfReader');
        
    }
    
    public function actionCreate() {
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->showPageTitle = false;
        $this->activeTab = 1;
        $this->activeMenu = self::MENU_EXAM_CREATE;
        $this->pageTitle = Lang::t('Create exam');
        $model = new Exams();
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
           //var_dump($_POST[$model_class_name]);die;
            
            if ($model->save()) {
                 
                
                Yii::app()->user->setFlash('success', Lang::t('Call saved successfully'));
                $this->redirect(array("index"));
            }
        }
        $this->render('create', array('model' => $model
        ));
    }
    public function actionUpdate($id)
        {
                $this->hasPrivilege(Acl::ACTION_UPDATE);
                $this->pageTitle = Lang::t('Edit ' . $this->resourceLabel);

                $model = Exams::model()->loadModel($id);
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                       // var_dump($model->attributes);die;
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('index'));
                        }
                }

                $this->render('update', array(
                    'model' => $model,
                ));
        }
    public function actionAdd() {
        $model = new Examtypes();
        $model_class_name = get_class($model);
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->save()) {
                if (Yii::app()->request->isAjaxRequest) {
                    echo CJSON::encode(array(
                        'status' => 'success',
                        'div' => "Exam type added",
                        'options' => "<option value=' $model->id ' selected='selected'>$model->description</option>"
                    ));
                    exit;
                } else
                    $this->redirect($this->actionCreate());
            }
            else {

                //echo 'could not insert';
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array(
                'status' => 'failure',
                'div' => $this->renderPartial('_examtype', array('model' => $model), true)));
            exit;
        } else
            $this->render('_examtype', array('model' => $model,));
    }
    public function actionVer() {
        $model = new Examversion();
        $model_class_name = get_class($model);
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->save()) {
                if (Yii::app()->request->isAjaxRequest) {
                    echo CJSON::encode(array(
                        'status' => 'success',
                        'div' => "Exam version added",
                        'options' => "<option value=' $model->id ' selected='selected'>$model->description</option>"
                    ));
                    exit;
                } else
                    $this->redirect($this->actionCreate());
            }
            else {

                //echo 'could not insert';
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array(
                'status' => 'failure',
                'div' => $this->renderPartial('_examversion', array('model' => $model), true)));
            exit;
        } else
            $this->render('_examversion', array('model' => $model,));
    }

    
}

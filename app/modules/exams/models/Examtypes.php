<?php

/**
 * This is the model class for table "tbl_content_type".
 *
 * The followings are the available columns in table 'tbl_content_type':
 * @property integer $id
 * @property string $description
 * @property string $status
 *
 * The followings are the available model relations:
 * @property TblContent[] $tblContents
 */
class Examtypes extends ActiveRecord implements IMyActiveSearch
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_content_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description', 'required'),
			array('description', 'length', 'max'=>100),
			array('status', 'length', 'max'=>25),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblContents' => array(self::HAS_MANY, 'TblContent', 'type_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'description' => 'Description',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchParams() {
        return array(
            array('package_code', self::SEARCH_FIELD, true, 'OR'),
            array('description', self::SEARCH_FIELD, true, 'OR'),
            //array('buying_center', self::SEARCH_FIELD, true, 'OR'),
            
        );
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Examtypes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

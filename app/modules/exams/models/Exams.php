<?php

/**
 * This is the model class for table "tbl_content".
 *
 * The followings are the available columns in table 'tbl_content':
 * @property integer $id
 * @property integer $source_id
 * @property string $year
 * @property string $price
 * @property string $title
 * @property string $status
 * @property string $upload
 * @property integer $subject_id
 * @property integer $type_id
 * @property integer $sample_exam
 * @property integer $publish
 * @property integer $publish_order
 * @property integer $content_version_id
 * @property string $description
 * @property string $date_created
 *
 * The followings are the available model relations:
 * @property AccessLogs[] $accessLogs
 * @property TblContentVersion $contentVersion
 * @property TblContentType $type
 * @property TblContentSources $source
 * @property TblSubjects $subject
 
 */
class Exams extends ActiveRecord implements IMyActiveSearch 
{
	/**
	 * @return string the associated database table name
	 */
    
        const BASE_DIR = 'exams';
	public function tableName()
	{
		return 'tbl_content';
	}
        
        public  $temp_profile_image;
        public  $date_created;

        
        /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('source_id, title, year, subject_id, type_id, content_version_id', 'required'),
			array('description', 'required' , 'message' => Lang::t('{attribute} for the exam is required ')),
			array('source_id, subject_id, type_id, sample_exam, publish, publish_order, content_version_id', 'numerical', 'integerOnly'=>true),
			array('year, title, upload', 'length', 'max'=>255),
			array('price, ', 'numerical', 'integerOnly' => false , 'message' => Lang::t('Please enter {attribute} in the format 25.50')),
			array('status', 'length', 'max'=>25),
			array('description', 'length', 'max'=>255),
                        array('description, price,temp_profile_image', 'safe'),
			array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accessLogs' => array(self::HAS_MANY, 'AccessLogs', 'content_id'),
			'contentVersion' => array(self::BELONGS_TO, 'TblContentVersion', 'content_version_id'),
			'type' => array(self::BELONGS_TO, 'TblContentType', 'type_id'),
			'source' => array(self::BELONGS_TO, 'TblContentSources', 'source_id'),
			'subject' => array(self::BELONGS_TO, 'TblSubjects', 'subject_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'source_id' => 'Source',
			'year' => 'Year',
			'price' => 'Price',
			'title' => 'Exam title',
			'status' => 'Status',
			'upload' => 'Upload Exam: [ pdf only ]',
			'subject_id' => 'Subject',
			'type_id' => 'Exam type',
			'sample_exam' => 'Sample Exam',
			'publish' => 'Published?',
			'publish_order' => 'Publish Order',
			'content_version_id' => 'Exam Version',
			'description' => 'Description',
			'date_created' => 'Date Created',
		);
	}
        public function beforeSave()
        {
            $this->date_created = date('Y-m-d h:i:s');
            $this->setProfileImage();
            if($this->publish == 1)
                {
            $this->orderPublish($this->publish_order);
                }
                else if((!$this->isNewRecord) && ($this->publish <> 1))
                     $this->unPublish($this->id);
            return parent::beforeSave();
        }

        
        protected function orderPublish($id)
        {
            $this->publish_order = $id;
            $published = Yii::app()->db->createCommand()
            ->select('id,publish_order')
            ->from($this->tableName())
            ->where('publish=1 AND publish_order >= :id', array(':id'=>$id))
            ->order('publish_order')
            ->queryAll();
            $id += 1;
        foreach ($published as $p):
        
        $model =  Exams::model()->loadModel($p['id']);
        $model->publish_order = $id;
        $model->save(FALSE);
        $id++;
        endforeach;
    
        }
        protected function unPublish($id)
        {
            $this->publish_order = '';
            $published = Yii::app()->db->createCommand()
            ->select('id,publish_order')
            ->from($this->tableName())
            ->where('publish=1 AND publish_order > (SELECT publish_order from '.$this->tableName().' WHERE  id = :id)', array(':id'=>$id))
            ->order('publish_order')
            ->queryAll();
            
        foreach ($published as $p):
        
        $model =  Exams::model()->loadModel($p['id']);
        $model->publish_order = $p['publish_order'] - 1;
        $model->save(FALSE);
       endforeach;
    
        }
        /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchParams() {
        return array(
            array('package_code', self::SEARCH_FIELD, true, 'OR'),
            array('description', self::SEARCH_FIELD, true, 'OR'),
            //array('buying_center', self::SEARCH_FIELD, true, 'OR'),
            
        );
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Exams the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getDir($id = null)
        {
                if (!empty($this->id))
                        $id = $this->id;
                return Common::createDir($this->getBaseDir() . DS . $id);
        }

        public function getBaseDir()
        {
                return Common::createDir(PUBLIC_DIR . DS . self::BASE_DIR);
        }

        public function getProfileImagePath($id = null)
        {
                if (!empty($this->id))
                        $id = $this->id;
                $exam_image = !empty($this->id) ? $this->upload : $this->get($id, 'upload');
                if (!empty($exam_image)) {
                        $file_path = $this->getDir($id) . DS . $exam_image;

                        if (file_exists($file_path))
                                return $file_path;
                }
                return $this->getDefaultProfileImagePath();
        }

        protected function getDefaultProfileImagePath()
        {
                return Yii::getPathOfAlias('webroot.themes') . DS . Yii::app()->theme->name . DS . 'images' . DS . 'default-profile-image.png';
        }

        public function setProfileImage()
        {
                //using fineuploader
           
                if (!empty($this->temp_profile_image)) {
                        $temp_file = Common::parseFilePath($this->temp_profile_image);
                        $file_name = $temp_file['name'];
                        $temp_dir = $temp_file['dir'];
                        if (copy($this->temp_profile_image, $this->getDir() . DS . $file_name)) {
                                if (!empty($temp_dir))
                                        Common::deleteDir($temp_dir);
                                $this->upload = $file_name;
                                
                                $this->temp_profile_image = null;
                        }
                }
        }
}

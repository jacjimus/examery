<?php if ($this->showLink(UserResources::RES_EXAMS)): ?>
        <li  class="<?php echo $this->getModuleName() === 'exams' ? 'active open' : '' ?>">
                <a href="#" class="dropdown-toggle">
                        <i class="icon-phone-sign"></i>
                        <span class="menu-text"> <?php echo Lang::t('Exams') ?></span>
                         <b class="arrow icon-angle-down"></b>
                </a>
            <ul class="submenu">
                        <?php if ($this->showLink(UserResources::RES_EXAMS_CREATE)): ?>
                                <li class="<?php echo $this->activeMenu === ExamsModuleController::MENU_EXAM_CREATE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_EXAMS . '/default/') ?>"><i class="icon-mail-forward"></i><?php echo Lang::t('Manage Exams') ?></a></li>
                        <?php endif; ?>
                        
                       
                        
                </ul>
        </li>
<?php endif; ?>

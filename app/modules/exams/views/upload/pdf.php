<?php

// Set up PDF headers
              header('Content-type: application/pdf');
              header('Content-Disposition: inline; filename="' . $filename . '"');
              header('Content-Transfer-Encoding: binary');
              header('Content-Length: ' . filesize($filepath));
              header('Accept-Ranges: bytes');

              // Render the file
              readfile($filepath);
          

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<?php echo $form->errorSummary($model) ?>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'title', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'title', array('class' => 'form-control', 'maxlength' => 100)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'subject_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
             <?php echo $form->dropDownList($model, 'subject_id', Subjects::model()->getListData('id' , 'description' , true), array('class' => 'form-control')); ?>
                   
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'source_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->dropDownList($model, 'source_id', Sources::model()->getListData('id' , 'description' , true ), array('class' => 'col-md-8' , 'id' => 'source_id')); ?>       
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'type_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->dropDownList($model, 'type_id', Examtypes::model()->getListData('id' , 'description' , true ), array('class' => 'col-md-8' , 'id' => 'type_id')); ?>
            &nbsp;&nbsp;&nbsp;&nbsp;<?php if ($this->showLink($this->resource, Acl::ACTION_CREATE)): ?><a class="" href="#" style = 'cursor: pointer;' onclick = "{addAcc(); $('#dialogAccount').dialog('open');}"><i class="icon-plus-sign"></i> <?php echo Lang::t('Create Exam type') ?></a><?php endif; ?>
               
        </div>
    </div>
    
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'year', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
             <?php echo CHtml::activeTextField($model, 'year', array('class' => 'form-control ')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'price', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
             <?php echo CHtml::activeTextField($model, 'price', array('class' => 'form-control ')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'content_version_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->dropDownList($model, 'content_version_id', Examversion::model()->getListData('id' , 'description' , true ), array('class' => 'col-md-7' , 'id' => 'content_version_id')); ?>
            &nbsp;&nbsp;&nbsp;&nbsp;<?php if ($this->showLink($this->resource, Acl::ACTION_CREATE)): ?><a class="" href="#" style = 'cursor: pointer;' onclick = "{addVer(); $('#dialogVer').dialog('open');}"><i class="icon-plus-sign"></i> <?php echo Lang::t('Create Exam Version') ?></a><?php endif; ?>
               
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextArea($model, 'description', array('class' => 'form-control', 'cols' => 20 , 'rows' => 4)); ?>
        </div>
    </div>
    
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'upload', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $this->renderPartial('_image_field' ,array('model' => $model))?>
        </div>
    </div>
    
    
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'publish', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeCheckBox($model, 'publish'  ,array('value' => '1', 'onchange' => 'javascript:$("#publish_order").toggle();javascript:$("#myclass").toggle()')); ?>
             <?php echo CHtml::activeLabelEx($model, 'publish_order', array('class' => 'control-label' , 'id' => 'myclass' )); ?>
                <?php echo CHtml::activeTextField($model, 'publish_order', array('class' => '' ,'id' => 'publish_order', 'value' => $model->isNewRecord ? 1 : $model->publish_order)); ?>
        </div>
    </div>
    
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'sample_exam', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeCheckBox($model, 'sample_exam'); ?>
        </div>
    </div>
    
    
    
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
   </div>
<?php $this->endWidget(); ?>

<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogAccount',
    'options' => array(
        'title' => 'Create Exam Type',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 350,
    ),
));
?>
<div class="divAcc"></div>

<?php $this->endWidget(); ?>
                       
<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogVer',
    'options' => array(
        'title' => 'Create Exam Version',
        'autoOpen' => false,
        'modal' => false,
        'width' => 500,
        'height' => 350,
    ),
));
?>
<div class="divVer"></div>

<?php $this->endWidget(); ?>
                       

<script type="text/javascript">
   

    function addAcc()
    {
<?php
echo CHtml::ajax(array(
    'url' => array("default/add"),
    'data' => "js:$(this).serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#dialogAccount div.divAcc form').submit(addAcc);
                }
                else
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                    setTimeout(\"$('#dialogAccount').dialog('close') \",400);
                    $('#type_id').append(data.options);
                }
 
            } ",
))
?>;
        return false;

    }
    
    function addVer()
    {
<?php
echo CHtml::ajax(array(
    'url' => array("default/ver"),
    'data' => "js:$(this).serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogVer div.divVer').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#dialogVer div.divVer form').submit(addVer);
                }
                else
                {
                    $('#dialogVer div.divVer').html(data.div);
                    setTimeout(\"$('#dialogVer').dialog('close') \",400);
                    $('#content_version_id').append(data.options);
                }
 
            } ",
))
?>;
        return false;

    }
    <?php
    if($model->isNewRecord || $model->publish != 1):
    ?>
   $(document).ready(function(){
 
      $('#publish_order').hide();
      $('#myclass').hide();
    
});
<?php endif;?>
</script>
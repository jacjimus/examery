<?php
$this->breadcrumbs = array(
    Lang::t($this->resourceLabel) => array('index'),
    $this->pageTitle,
);
?>
<div class="widget-box">
    <div class="widget-header">
        <h4><?php echo CHtml::encode(Lang::t('Preview the Exam Content  - ')); ?></h4>
        <div class="widget-toolbar">
            <a href="<?php echo $this->createUrl('index') ?>"><i class="icon-remove"></i> <?php echo Lang::t('Cancel') ?></a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-main">
<?php
$this->widget('ext.pdfJs.QPdfJs', array(
    'url' => PUBLIC_DIR . '/exams/test.pdf'));


?>
            

        </div>
    </div>
</div>
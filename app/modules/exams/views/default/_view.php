<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <i class="fa fa-certificate"></i> <?php echo Lang::t('Exam Details') ?>
<?php if (Controller::showlink($this->resource , Acl::ACTION_UPDATE)): ?>
                                <span><a class="pull-right white" href="<?php echo $this->createUrl('update', array('id' => $model->id)) ?>"><i class="fa fa-edit"></i> <?php echo Lang::t('Edit Exam') ?></a></span>
                        <?php endif; ?>
        </h4>
    </div>
    <div id="exam_info" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="detail-view">
                <?php
                $this->widget('application.components.widgets.DetailView', array(
                    'data' => $model,
                    'attributes' => array(
                        array(
                            'name' => 'title',
                        ),
                         array(
                            'name' => 'subject_id',
                            'type' => 'raw',
                            'value' => Subjects::model()->get($model->subject_id , "description")
                        ),
                        array(
                            'name' => 'source_id',
                            'type' => 'raw',
                            'value' => Sources::model()->get($model->source_id , "description")
                        ),
                        array(
                            'name' => 'type_id',
                            'type' => 'raw',
                            'value' => Examtypes::model()->get($model->type_id , "description")
                        ),
                        array(
                            'name' => 'year',
                        ),
                        array(
                            'name' => 'price',
                        ),
                        array(
                            'name' => 'content_version_id',
                            'type' => 'raw',
                            'value' => Examversion::model()->get($model->content_version_id, "description")
                        ),
                        array(
                            'name' => 'publish',
                            'type' => 'raw',
                            'value' => $model->publish==1?"True":"False",
                        ),
                        array(
                            'name' => 'publish_order',
                            'type' => 'raw',
                            'visible' => $model->publish==1?True:False,
                            'value' => $model->publish_order,
                        ),
                        array(
                            'name' => 'sample_exam',
                            'type' => 'raw',
                            'value' => $model->sample_exam==1?"True":"False",
                        ),
                        array(
                            'name' => 'date_created',
                            'type' => 'raw',
                            'value' =>  MyYiiUtils::formatDate($model->date_created),
                        ),
                        array(
                            'name' => 'status',
                            'type' => 'raw',
                            'value' => CHtml::tag('span', array('class' => $model->status === Users::STATUS_ACTIVE ? 'badge badge-success' : 'badge badge-danger'), $model->status),
                        ),
                        array(
                            'name' => 'upload',
                            'type' => 'raw',
                            'value' => CHtml::link( $model->upload , array('exam' ,'id'=>$model->id , 'file' => $model->upload)),
                        ),
                        array(
                            'name' => 'description',
                            'type' => 'raw',
                            'value' => $model->description,
                        ),
                )
                    )
                        );
                ?>
            </div>
        </div>
        </div>
     
</div>


<?php

/**
 *
 * @author James Makau <jacjimus@gmail.com>
 */
class ExamsModuleController extends Controller {

        const MENU_EXAM_CREATE = 'EXAM_CREATE';
        const MENU_VERSION = 'VERSION';
        const MENU_TYPES = 'TYPES';
        const MENU_SOURCES = 'SOURCES';
        const MENU_WAITING = 'WAITING';
        const MENU_PACKAGE = 'PACKAGE';
        const MENU_SUBJECT = 'SUBJECT';
        const MENU_BUDDLE = 'BUDDLE';

        public function init()
        {
                parent::init();
        }

}

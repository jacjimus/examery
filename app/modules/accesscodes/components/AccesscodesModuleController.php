<?php

/**
 *
 * @author James Makau <jacjimus@gmail.com>
 */
class AccesscodesModuleController extends Controller {

        const MENU_CREATE = 'Generate';
        const MENU_ACTIVATE_DEACTIVATE = 'Activate';
        const MENU_ORDERS = 'Orders';
        const MENU_SETUP = 'Setup';
        const resourceBulkLabel = 'Access Codes';

        public function init()
        {
                parent::init();
        }
        

}

<?php if ($this->showLink(UserResources::RES_ACCESS_CODES)): ?>
        <li  class="<?php echo $this->getModuleName() === 'accesscodes' ? 'active open' : '' ?>">
                <a href="#" class="dropdown-toggle">
                        <i class="icon-comments"></i>
                        <span class="menu-text"> <?php echo Lang::t('Access Codes') ?></span>
                         <b class="arrow icon-angle-down"></b>
                </a>
            <ul class="submenu">
                        <?php if ($this->showLink(UserResources::RES_ACCESS_CODES_CREATE)): ?>
                <li class="<?php echo $this->activeMenu === AccesscodesModuleController::MENU_CREATE ? 'active' : '' ?>">
                    <a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_ACCESS_CODES . '/default/') ?>">
                        <i class="icon-double-angle-right"></i><?php echo Lang::t('Manage Access Codes') ?></a>
                </li>
                        <?php endif; ?>
                        
                                
                        <?php if ($this->showLink(UserResources::RES_ACCESS_CODES_ACTIVATE)): ?>
                                <li class="<?php echo $this->activeMenu === AccesscodesModuleController::MENU_ACTIVATE_DEACTIVATE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_ACCESS_CODES . '/default/template') ?>"><i class="icon-briefcase"></i><?php echo Lang::t('Activate / Deactivate Access') ?></a></li>
                        <?php endif; ?>
                        <?php if ($this->showLink(UserResources::RES_ACCESS_CODES_ORDERS)): ?>
                                <li class="<?php echo $this->activeMenu === AccesscodesModuleController::MENU_ORDERS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl(ModulesEnabled::MOD_ACCESS_CODES . '/buddles/') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Manage Orders') ?></a></li>
                        <?php endif; ?>
                        <?php if ($this->showLink(UserResources::RES_ACCESS_CODES_SETUP)): ?>
                                <li class="<?php echo $this->activeMenu === AccesscodesModuleController::MENU_SETUP ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('users/resources/index') ?>"><i class="icon-double-angle-right"></i><?php echo Lang::t('Access Codes Setup') ?></a></li>
                        <?php endif; ?>
                        
                       
                        
                </ul>
        </li>
<?php endif; ?>


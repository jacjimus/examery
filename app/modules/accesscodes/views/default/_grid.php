<?php

$grid_id = 'access-codes-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => $this->pageTitle,
    'titleIcon' => null,
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
           
           
            array(
                'name' => 'access_code',
            ),
            array(
                'name' => 'login_count',
            ),
            array(
                'name' => 'order_no',
            ),
            array(
                'name' => 'status',
            ),
           
            array(
                'class' => 'ButtonColumn',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'template' => '{update}{trash}',
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->access_code))',
                        'visible' => '$this->grid->owner->showLink("' . $this->resource . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'trash' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->access_code))',
                        'visible' => '$this->grid->owner->showLink("' . $this->resource . '","' . Acl::ACTION_DELETE . '")?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
<?php
$this->breadcrumbs = array("Bulk Groups" => 'groups',
    $this->pageTitle,
);
?>
<div class="widget-box">
    <div class="widget-header">
        <h4><?php echo Lang::t('Add phone numbers to group') ?></h4>

    </div>
    <div class="widget-body">
        <div class="widget-main" style="padding: 0px !important;">
            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'registration-form', 'enableAjaxValidation' => true,
                'htmlOptions' => array('enctype' => 'multipart/form-data'),));
            ?>
            <div class="form-group"  style="padding: 10px">
                <?php echo $form->labelEx($model, 'csv_file', array('class' => "col-lg-2 control-label", 'label' => Lang::t('Attach Csv File:'))); ?>


                <div class="col-lg-5" style="padding: 10px;">
                    
                    <?php echo $form->fileField($model, 'csv_file', array("class" => "btn-grey")); ?> 
                    <?php echo $form->error($model, 'csv_file'); ?>
                </div>
                <button class="btn btn-success" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t('Upload Csv') ?></button>
            </div>


            <?php
            $this->endWidget();
            ?>
        </div>
    </div>
</div>
<?php
if ($set) {
    $this->renderPartial('list', array('client' => $client));
}
<?php
$grid_id = 'groups-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<!--grid header-->
<div class="row grid-view-header">
        <div class="col-sm-6">
                <div class="btn-group">
                        <?php if ($this->showLink($this->resource, Acl::ACTION_CREATE)): ?><a class="btn btn-sm" href="<?php echo $this->createUrl('add') ?>"><i class="icon-plus-sign"></i> <?php echo Lang::t('Add Bulk Sms Group') ?></a><?php endif; ?>
                </div>
        </div>
        <div class="col-sm-6">
                <div class="dataTables_filter">
                        <?php
                        $this->beginWidget('ext.activeSearch.AjaxSearch', array(
                            'gridID' => $grid_id,
                            'formID' => $search_form_id,
                            'model' => $model,
                            'action' => Yii::app()->createUrl($this->route, $this->actionParams),
                        ));
                        ?>
                        <?php $this->endWidget(); ?>
                </div>
        </div>
</div>
<?php
$this->widget('application.components.widgets.GridView', array(
    'id' => $grid_id,
    'dataProvider' => $model->search(),
    'enablePagination' => $model->enablePagination,
    'enableSummary' => $model->enableSummary,
    'columns' => array(
        
         array(
            'name' => 'group_name',
            'visible' => true,
        ),
        array(
            'name' => 'number',
            'header' => 'Numbers',
            'value' => '$data->number',
        ),
        
        
        array(
            'class' => 'ButtonColumn',
            'template' => '{view}&nbsp;{update}&nbsp;{delete}',
            'htmlOptions' => array('class' => 'text-center', 'style' => 'width: 100px;'),
            'buttons' => array(
                'view' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-eye-open bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("attach",array("id"=>$data->primaryKey, "cl" => Yii::app()->user->client ))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_BULK_GROUP . '", "' . Acl::ACTION_UPDATE . '")?true:false',
                    'options' => array(
                        'class' => 'blue',
                        'title' => 'View / Add Numbers',
                    ),
                ),
                'update' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-pencil bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("updategrp",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_BULK_TEMPLATES . '", "' . Acl::ACTION_UPDATE . '")?true:false',
                    'options' => array(
                        'class' => 'green',
                        'title' => 'Edit',
                    ),
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-trash bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("deletegroup",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_BULK_TEMPLATES . '", "' . Acl::ACTION_DELETE . '")?true:false',
                    'options' => array(
                        'class' => 'delete red',
                        'title' => 'Delete',
                    ),
                ),
            )
        ),
    ),
));
?>
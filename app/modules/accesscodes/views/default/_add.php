<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'email-template-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form',
    )
        ));

if((Yii::app()->user->client != 0))
{
    echo $form->hiddenField($model, 'client', array('value' =>  (int) Yii::app()->user->client));
}
 else {
    ?>

   <div class="form-group">
                <?php echo $form->labelEx($model, 'client', array('class' => "col-lg-2 control-label", 'label' => Lang::t('Client'))); ?>
                <div class="col-lg-5">
                        <?php echo CHtml::activeDropDownList($model, 'client', Clients::model()->getListData('id', 'client_name',true,'',array(),'client_name'), array('class' => 'form-control')); ?>
                         <?php echo CHtml::error($model, 'client') ?>
                </div>
        </div>
 <?php } ?>  
<div class="form-group">
                <?php echo $form->labelEx($model, 'group_name', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
            <?php echo $form->textField($model, 'group_name', array('class' => 'form-control', 'maxlength' => 128)); ?>
                <?php echo CHtml::error($model, 'group_name') ?>
                </div>
        </div>

       

<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn" href="<?php echo $this->createUrl('groups') ?>"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
        </div>
</div>
<?php $this->endWidget(); ?>

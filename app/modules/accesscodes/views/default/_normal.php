<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'email-template-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form',
    )
        ));

if((Yii::app()->user->client != 0))
{
    echo $form->hiddenField($model, 'client', array('value' =>  (int) Yii::app()->user->client));
}
 else {
    ?>

   <div class="form-group">
                <?php echo $form->labelEx($model, 'client', array('class' => "col-lg-2 control-label", 'label' => Lang::t('Client'))); ?>
                <div class="col-lg-5">
                        <?php echo CHtml::activeDropDownList($model, 'client', Clients::model()->getListData('id', 'client_name',true,'',array(),'client_name'), array('class' => 'form-control')); ?>
                         <?php echo CHtml::error($model, 'client') ?>
                </div>
        </div>
 <?php } ?>  
<div class="form-group">
                <?php echo $form->labelEx($model, 'sms_group', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
            <?php 
            $client =   (Yii::app()->user->client != 0)? "client = ". (int) Yii::app()->user->client : "";
                
            echo CHtml::activeDropDownList($model, 'sms_group', SmsGroup::model()->getListData('id', 'group_name',true, $client, array(),'group_name'), array('class' => 'form-control')); ?>
                         <?php echo CHtml::error($model, 'sms_group') ?>
                </div>
</div>


<div class="form-group">
        <?php echo $form->labelEx($model, 'text_message', array('class' => 'col-lg-2 control-label', 'id' => 'text_message')); ?>
        <div class="col-lg-5">
            <?php echo CHtml::error($model, 'text_message') ?>
          <?php echo $form->hiddenField($model, 'send_time', array('value' =>  time())) ?>
                
        
<div id="charNum"></div>
                <?php echo $form->textArea($model, 'text_message', array('class' => 'form-control redactor', 'rows' => 6, 'cols' => 30, 'maxLength' => 210, 'onkeyup'=>'countChar(this)',)); ?>
                
        </div>
</div>
<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9"  id="send_bulk">
            <button class="btn btn-primary" type="submit"><i class="icon-briefcase bigger-110"></i> <?php echo Lang::t('Send Bulk SMSes') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn" href="<?php echo $this->createUrl('index') ?>"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
        </div>
</div>
<?php $this->endWidget(); ?>
<?php

?>
<script>
     
      $('#characterLeft').text('210 characters left');
      function countChar(val) {
        var len = val.value.length;
        var max = 210
        if (len >= max) {
          $('#characterLeft').text(' you have reached the limit');
          val.value = val.value.substring(0, max);
        } else {
          var ch= max - len;
          $('#charNum').text(  ch + ' characters left');
        }
      };
      
  
  
    </script>

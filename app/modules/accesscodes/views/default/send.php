<div class="widget-box transparent">
        <div class="widget-header">
                <?php echo $this->renderPartial('bulk.views.default._tab') ?>
        </div>
        <div class="widget-box">
        <div class="widget-header">
                <h4><?php echo CHtml::encode($this->pageTitle); ?></h4>
                <div class="widget-toolbar">
                        <a href="<?php echo $this->createUrl('template') ?>"><i class="icon-remove"></i> <?php echo Lang::t('Cancel') ?></a>
                </div>
        </div>
        <div class="widget-body">
            
            <div class="widget-main" id="bulk_sms">
                    
                        <?php $this->renderPartial('_normal', array('model' => $model)); ?>
                </div>
        </div>
</div>
</div>


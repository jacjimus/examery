<?php
if($send->hasErrors()){
  echo CHtml::errorSummary($send);
}
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'email-template-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
        'role' => 'form',
        'enctype' => 'multipart/form-data'
        
    )
        ));

if((Yii::app()->user->client != 0))
{
    echo $form->hiddenField($model, 'client', array('value' =>  (int) Yii::app()->user->client));
}
 else {
    ?>

   <div class="form-group">
                <?php echo $form->labelEx($model, 'client', array('class' => "col-lg-2 control-label", 'label' => Lang::t('Client'))); ?>
                <div class="col-lg-5">
                        <?php echo CHtml::activeDropDownList($model, 'client', Clients::model()->getListData('id', 'client_name',true,'',array(),'client_name'), array('class' => 'form-control')); ?>
                         <?php echo CHtml::error($model, 'client') ?>
                </div>
        </div>
 <?php } ?>  


<div class="form-group">
                <?php echo $form->labelEx($model, 'template', array('class' => 'col-lg-2 control-label')); ?>
                <div class="col-lg-5">
            <?php 
            $client =   (Yii::app()->user->client != 0)? "client_id = ". (int) Yii::app()->user->client : "";
                
            echo CHtml::activeDropDownList($model, 'template', BulkTemplate::model()->getListData('id', 'alias',true, $client, array(),'alias'), array('class' => 'form-control')); ?>
                         <?php echo CHtml::error($model, 'template') ?>
                </div>
</div>



<div class="form-group">
                <?php echo $form->labelEx($model, 'csv_file', array('class' => "col-lg-2 control-label", 'label' => Lang::t('Attach Csv File:'))); ?>


                <div class="col-lg-5">

                    <?php echo $form->fileField($model, 'csv_file', array("class" => "btn-grey")); ?> 
                    <?php echo $form->error($model, 'csv_file'); ?>
                </div>
 </div>
<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn" href="<?php echo $this->createUrl('index') ?>"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
        </div>
</div>
<?php $this->endWidget(); 

$this->beginWidget('zii.widgets.jui.CJuiDialog',
    array(
        'id'=>'mywaitdialog',
        'options'=>array(
            'title'=>'My Title',
            'modal'=>true,
            'autoOpen'=>false,// default is true
            'closeOnEscape'=>false,
            'open'=>// supply a callback function to handle the open event
                    'js:function(){ // in this function hide the close button
                         $(".class-of-closebutton").hide();
                    }'
         ))
);
 $this->endWidget(); 
<?php
$grid_id = 'phone-grid';
$search_form_id = $grid_id . '-active-search-form';
$client = "sms_group = '$client'";
 $model = Phonebook::model()->searchModel(array(), 100, 'id' , $client);
?>
<div class="row grid-view-header">
        <div class="col-sm-6">
                
        </div>
        <div class="col-sm-6">
                <div class="dataTables_filter">
                        <?php
                        $this->beginWidget('ext.activeSearch.AjaxSearch', array(
                            'gridID' => $grid_id,
                            'formID' => $search_form_id,
                            'model' => $model,
                            'action' => Yii::app()->createUrl($this->route, $this->actionParams),
                        ));
                        ?>
                        <?php $this->endWidget(); ?>
                </div>
        </div>
</div>
<?php

 
 $this->widget('application.components.widgets.GridView', array(
    'id' => $grid_id,
    'dataProvider' => $model->search(),
    'enablePagination' => $model->enablePagination,
    'enableSummary' => $model->enableSummary,
    'columns' => array(
        
        array(
            'name' => 'phone_number',
            ),
        
        array(
            'name' => 'group',
        ),
        
        array(
            'class' => 'ButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'imageUrl' => false,
                    'label' => '<i class="icon-trash bigger-130"></i>',
                    'url' => 'Yii::app()->controller->createUrl("deletenum",array("id"=>$data->primaryKey))',
                    'visible' => '$this->grid->owner->showLink("' . UserResources::RES_BULK_GROUP . '", "' . Acl::ACTION_DELETE . '")?true:false',
                    'options' => array(
                        'class' => 'delete red',
                        'title' => 'Delete',
                    ),
                ),
                
            )
        ),
    ),
));
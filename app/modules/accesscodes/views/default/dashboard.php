
<div class="widget-box transparent">
        <div class="widget-header">
                <?php echo $this->renderPartial('bulk.views.default._tab') ?>
        </div>
        <div class="widget-box">
        <div class="widget-header">
                <h4><?php echo CHtml::encode($this->pageTitle); ?></h4>
                
        </div>
        <div class="widget-body">
                <div id="personal_info" class="panel-collapse collapse in">
                <div class="panel-body">
                        <div class="detail-view">
                                <?php
                                $this->widget('application.components.widgets.DetailView', array(
                                    'data' => $model,
                                    'attributes' => array(
                                        array(
                                            'name' => 'client',
                                        ),
                                        
                                        array(
                                            'name' => 'send_year',
                                        ),
                                        array(
                                            'name' => 'send_prev',
                                        ),
                                        array(
                                            'name' => 'send',
                                        ),
                                        
                                        array(
                                            'name' => 'units',
                                            'type' => 'raw',
                                            'value' =>CHtml::tag("span", array("class"=>$model->units < 100 ?"badge badge-success":"badge badge-danger"), $model->units),
                                        ),
                                        
                                    ),
                                ));
                                ?>
                        </div>
                </div>
        </div>
        </div>
</div>
</div>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<?php echo $form->errorSummary($model) ?>
<div class="modal-body">
    <div class="alert hidden" id="my-modal-notif"></div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'order_no', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
             <?php echo CHtml::activeTextField($model, 'order_no', array('class' => 'form-control ', 'value' => $batch , 'readonly' => true)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'group_name', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'group_name', array('class' => 'form-control', 'maxlength' => 20)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'buying_center', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'buying_center', array('class' => 'form-control', 'maxlength' => 20)); ?>
        </div>
    </div>
    
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'sessions', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'sessions', array('class' => 'form-control', 'maxlength' => 20)); ?>
        </div>
    </div>
    
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'no_of_cards', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'no_of_cards', array('class' => 'form-control', 'maxlength' => 20)); ?>
        </div>
    </div>
    
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'package_code', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo $form->dropDownList($model, 'package_code', Packages::model()->getListData('package_code' , 'description' , true , 'status = ' . Packages::STATUS_ACTIVE), array('class' => 'select2')); ?>
            <?php if ($this->showLink($this->resource, Acl::ACTION_CREATE)): ?><a class="" href="#" style = 'cursor: pointer;' onclick = "{addAcc(); $('#dialogAccount').dialog('open');}"><i class="icon-plus-sign"></i> <?php echo Lang::t('Add Budddle Package') ?></a><?php endif; ?>
               
        </div>
    </div>
    
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
   </div>
<?php $this->endWidget(); ?>

<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogAccount',
    'options' => array(
        'title' => 'Create Buddle Package',
        'autoOpen' => false,
        'modal' => false,
        'width' => 550,
        'height' => 400,
    ),
));
?>
<div class="divAcc"></div>

<?php $this->endWidget(); ?>
                       

<script type="text/javascript">
   

    function addAcc()
    {
<?php
echo CHtml::ajax(array(
    'url' => array("buddles/add"),
    'data' => "js:$(this).serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#dialogAccount div.divAcc form').submit(addAcc);
                }
                else
                {
                    $('#dialogAccount div.divAcc').html(data.div);
                    setTimeout(\"$('#dialogAccount').dialog('close') \",400);
                    $('#package_code').append(data.options);
                }
 
            } ",
))
?>;
        return false;

    }
    
   
</script>
<?php

/**
 * This is the model class for table "tbl_buddle_orders".
 *
 * The followings are the available columns in table 'tbl_buddle_orders':
 * @property string $order_no
 * @property string $group_name
 * @property string $buying_center

 * @property integer $sessions
 * @property integer $package_code
 * @property integer $no_of_cards
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property TblAccessCodes[] $tblAccessCodes
 */
class Buddleorders extends ActiveRecord implements IMyActiveSearch
{
	/**
	 * @return string the associated database table name
	 */
    public $status;
	public function tableName()
	{
		return 'tbl_buddle_orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('order_no , no_of_cards , sessions, group_name , buying_center, package_code', 'required'),
			array('sessions, no_of_cards, status', 'numerical', 'integerOnly'=>true),
			array('order_no, group_name', 'length', 'max'=>25),
			array('buying_center', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblAccessCodes' => array(self::HAS_MANY, 'TblAccessCodes', 'order_no'),
		);
	}
        public function afterFind()
        {
            $this->status = $this->status == 0 ? 'Pending' : 'Generated';
             return parent::afterFind();
        }

        /**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'order_no' => 'Batch No',
			'group_name' => 'Group Name',
			'buying_center' => 'Buying Center',
			
			'sessions' => 'Sessions per Card',
			'package_code' => 'Subject Package',
			'no_of_cards' => 'No of Cards',
			'status' => 'Status',
		);
	}

	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Buddleorders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
         public function searchParams()
    {
        return array(
            array('order_no', self::SEARCH_FIELD, true, 'OR'),
            array('group_name', self::SEARCH_FIELD, true, 'OR'),
            array('buying_center', self::SEARCH_FIELD, true, 'OR'),
            array('sessions', self::SEARCH_FIELD, true, 'OR'),
            array('status', self::SEARCH_FIELD, true, 'OR'),
           
        );
    }
}

<?php

/**
 * This is the model class for table "settings_email_template".
 *
 * The followings are the available columns in table 'settings_email_template':
 * @property integer $id
 * @property string $key
 * @property string $description
 * @property string $subject
 * @property string $body
 * @property string $from
 * @property string $comments
 * @property string $date_created
 * @property integer $created_by
 */
class Phonebook extends ActiveRecord implements IMyActiveSearch {
        //email template keys

        public  $csv_file;
        public  $group;
        public  $num;
        const SCENARIO_NOT_ADMIN = 'n_admin';
         public $_search;

        const SEARCH_FIELD = '_search';
       
       

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'sdp.bulk_phonebook';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                     array('csv_file', 'file', 'types' => 'csv', 'maxSize'=>5242880, 'allowEmpty' => false, 'wrongType'=>'Only csv allowed.', 'tooLarge'=>'File too large! 5MB is the limit'), 
                     array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
        }
        
        public function afterFind()
        {
                $this->group = $this->getName($this->sms_group);
                return parent::afterFind(); 
        }
        
       
        /**
         * @return array relational rules.
         */
        public function relations()
        {
                return array(
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
                return array(
                    'id' => Lang::t('ID'),
                    'sms_group' => Lang::t('Sms Group'),
                    'phone_number' => Lang::t('Phone Number'),
                    
                );
        }

        public function searchParams()
        {
                return array(
                    array('id', self::SEARCH_FIELD, true, 'OR'),
                    array('phone_number', self::SEARCH_FIELD, true, 'OR'),
                    'id',
                );
        }

        /**
         * Find email template by key
         * @param type $key
         * @return type
         * @throws CHttpException
         */
        public function findByKey($key)
        {
                $model = $this->find('`phone_number`=:t1', array(':t1' => $key));
                if ($model === null)
                        throw new CHttpException(500, 'No phone with found: ' . $key);
                return $model;
        }
        
        public function getName($id)
        {
                if (empty($id))
                        return NULL;
                $name = SmsGroup::model()->getScaler('group_name', '`id`=:t1 ', array(':t1' => $id));
                return !empty($name) ? $name : NULL;
        }

}


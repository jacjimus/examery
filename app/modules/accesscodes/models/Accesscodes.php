<?php

/**
 * This is the model class for table "tbl_access_codes".
 *
 * The followings are the available columns in table 'tbl_access_codes':
 * @property string $status
 * @property integer $login_count
 * @property string $access_code
 * @property string $order_no
 *
 * The followings are the available model relations:
 * @property TblBuddleOrders $orderNo
 * @property TblContentPurchases[] $tblContentPurchases
 * @property TblLogins[] $tblLogins
 */
class Accesscodes extends ActiveRecord 
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_access_codes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('access_code', 'required'),
			array('login_count', 'numerical', 'integerOnly'=>true),
			array('status, access_code, order_no', 'length', 'max'=>25),
			array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			/**'orderNo' => array(self::BELONGS_TO, 'TblBuddleOrders', 'order_no'),
			'tblContentPurchases' => array(self::HAS_MANY, 'TblContentPurchases', 'access_code'),
			'tblLogins' => array(self::HAS_MANY, 'TblLogins', 'access_code'),
                         * **/
                         
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'status' => 'Status',
			'login_count' => 'Login Count',
			'access_code' => 'Access Code',
			'order_no' => 'Order No',
		);
	}

	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Accesscodes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
         
}

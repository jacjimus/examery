<?php

class BuddlesController extends AccesscodesModuleController {

        public function init()
        {
            $this->resource = UserResources::RES_ACCESS_CODES_ORDERS;
            $this->activeMenu = self::MENU_ORDERS;
            $this->resourceLabel = 'Scratch cards Orders';
            $this->resourceAddLabel = 'Scratch cards';
        parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete',
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                    array('allow',
                        'actions' => array('create' ,'index', 'add'),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

         public function actionCreate()
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);
       
        $model = new Buddleorders();
        $model_class_name = get_class($model);
        $batch = $this->generateBatchNo();

        
        $model_class_name = get_class($model);
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(array('index'));
            }
        }

        $this->render('create', array('model' => $model, 'batch' => $batch), false, true);
    }
    
     
        public function actionAdd() {
        $model = new Packages();
        $model_class_name = get_class($model);
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->save()) {
                if (Yii::app()->request->isAjaxRequest) {
                    echo CJSON::encode(array(
                        'status' => 'success',
                        'div' => "Package added",
                        'options' => "<option value=' $model->package_code ' selected='selected'>$model->description</option>"
                    ));
                    exit;
                } else
                    $this->redirect($this->actionCreate());
            }
            else {

                //echo 'could not insert';
            }
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array(
                'status' => 'failure',
                'div' => $this->renderPartial('_package', array('model' => $model), true)));
            exit;
        } else
            $this->render('_package', array('model' => $model,));
    }

    public function actionIndex()
        {

        $this->hasPrivilege(Acl::ACTION_VIEW);


        $this->showPageTitle = false;
        $this->activeMenu = self::MENU_ORDERS;
        $this->pageTitle = Lang::t('Manage Buddle orders');


        $model = Buddleorders::model()->find();
        //var_dump($model);die;
        $this->render('view', array('model' => $model
        ));
    }
    
    private function generateBatchNo()
    {
        $last = Yii::app()->db->createCommand('SELECT id FROM tbl_buddle_orders ORDER BY id DESC LIMIT 1')->queryScalar();
        
        $plus = $last + 1;
        if($plus < 10)
        $batch = sprintf("%05d",$plus);
        if($plus >= 10 && $plus < 100)
        $batch = sprintf("%04d",$plus);
        if($plus >= 100 && $plus < 1000)
        $batch = sprintf("%03d",$plus);
        if($plus >= 1000)
        $batch = sprintf("%02d",$plus);
        
        return $batch;
    }


}

        
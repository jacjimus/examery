<?php

class DefaultController extends AccesscodesModuleController {

    public function init() {
        $this->resource = UserResources::RES_ACCESS_CODES;
        $this->activeMenu = self::MENU_CREATE;
        $this->resourceLabel = 'Acess Codes';

        $this->resourceAddLabel = 'Access codes';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionCreate() {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $model = new Accesscodes();
        $model_class_name = get_class($model);

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = json_decode($error_message);
            if (!empty($error_message_decoded)) {
                echo json_encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(false);
                echo json_encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), false, true);
    }

    public function actionIndex() {

        $this->hasPrivilege(Acl::ACTION_VIEW);

        $this->showPageTitle = false;
        $this->activeTab = 0;
        $this->activeMenu = self::MENU_CREATE;
        $this->pageTitle = Lang::t('Manage Access codes');


        $model = Accesscodes::model()->find();
        //var_dump($model);die;
        $this->render('accesscodes', array('model' => $model
        ));
    }

}

<?php

class DefaultController extends AccesscodesModuleController {

        public function init()
        {
                
                parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete',
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                    array('allow',
                        'actions' => array('manage' , 'create' ,'buddles'),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

         public function actionCreate()
    {
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $model = new Accesscodes();
        $model_class_name = get_class($model);

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = json_decode($error_message);
            if (!empty($error_message_decoded)) {
                echo json_encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(false);
                echo json_encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('index'))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), false, true);
    }
         public function actionAdd()
        {
                $this->resource = UserResources::RES_ACCESS_CODES_CREATE;
               
        $this->hasPrivilege(Acl::ACTION_CREATE);
        $this->activeMenu = self::MENU_CREATE;

                $this->pageTitle = Lang::t('Add scratch card order');

                $model = new Buddleorders;
                $model_class_name = $model->getClassName();

                if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        if ($model->save()) {
                                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                                $this->redirect(array('attach?id='.$model->id."&cl=". $model->client));
                        }
                }

                $this->render('add', array(
                    'model' => $model,
                ));
        }
        
        public function actionManage()
        {
        $this->resource = UserResources::RES_ACCESS_CODES;
               
        $this->hasPrivilege(Acl::ACTION_VIEW);
        $this->activeMenu = self::MENU_CREATE;
        $this->resourceLabel = 'Acess Codes';

        $this->resourceAddLabel = 'Access codes';
        $this->showPageTitle = false;
        $this->activeTab = 0;
        $this->activeMenu = self::MENU_CREATE;
        $this->pageTitle = Lang::t('Manage Access codes');


        $model = Accesscodes::model()->find();
        //var_dump($model);die;
        $this->render('accesscodes', array('model' => $model
        ));
    }
        public function actionBuddles()
        {
        $this->resource = UserResources::RES_ACCESS_CODES_ORDERS;
        $this->hasPrivilege(Acl::ACTION_VIEW);
        
        $this->activeMenu = self::MENU_ORDERS;
        $this->resourceLabel = 'Scratch cards Orders';

        $this->resourceAddLabel = 'Scratch cards';
        $this->showPageTitle = false;
        $this->activeTab = 0;
        $this->activeMenu = self::MENU_ORDERS;
        $this->pageTitle = Lang::t('Manage Buddle orders');


        $model = Buddleorders::model()->find();
        //var_dump($model);die;
        $this->render('buddles', array('model' => $model
        ));
    }
        
        
}

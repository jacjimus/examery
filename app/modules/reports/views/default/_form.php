

<?php

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'custome-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => $model->isNewRecord == true ? "form-horizontal" : "form-vertical",
        'role' => 'form',
    )
        ));

?>  
<Br />
<div class="form-group">
                <?php echo $form->labelEx($model, 'name', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-9">
            <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'maxlength' => 100)); ?>
                <?php echo CHtml::error($model, 'name') ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'description', array('class' => 'col-lg-3 control-label')); ?>
                <div class="col-lg-9">
                        <?php echo $form->textArea($model, 'description', array('class' => 'form-control', 'maxlength' => 128)); ?>
                        <?php echo CHtml::error($model, 'description') ?>
                </div>
        </div>


<div class="form-group">
        <?php echo $form->labelEx($model, 'start_date', array('class' => 'col-lg-3 control-label')); ?>
        <div class="col-lg-8">
           <?php	
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
	   'model' => $model,
            'attribute' => 'start_date',
            'options'=>array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim'=>'fold',
                'changeMonth'=>true,
                'changeYear'=>true,
            ),
            'htmlOptions'=>array(
            'style'=>'width:250px;',
           
            ),

            ));?>
 <?php echo CHtml::error($model, 'start_date') ?>
        </div>
</div>
<div class="form-group">
        <?php echo $form->labelEx($model, 'end_date', array('class' => 'col-lg-3 control-label')); ?>
        <div class="col-lg-8">
           <?php	
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
	   'model' => $model,
            'attribute' => 'end_date',
            'options'=>array(
                'dateFormat' => 'yy-mm-dd',
                'showAnim'=>'fold',
                'changeMonth'=>true,
                'changeYear'=>true,
            ),
            'htmlOptions'=>array(
            'style'=>'width:250px;',
           
            ),

            ));?>
 <?php echo CHtml::error($model, 'end_date') ?>
        </div>
</div>

<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
                &nbsp; &nbsp; &nbsp;
                <a class="btn" href="#" onclick="$('#<?=$model->isNewRecord == true ? "dialogStaff" : "groups-dialog" ?>').dialog('close');"><i class="icon-remove bigger-110"></i><?php echo Lang::t('Cancel') ?></a>
        </div>
</div>
<?php $this->endWidget(); ?>

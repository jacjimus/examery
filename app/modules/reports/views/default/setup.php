<?php
$grid_id = 'custom-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<!--grid header-->
<div class="row grid-view-header">
        <div class="col-sm-6">
                <div class="btn-group">
                        <?php if ($this->showLink($this->resource, Acl::ACTION_CREATE)): ?><a class="btn btn-sm" href="#" style = 'cursor: pointer;' onclick = "{addStaff(); $('#dialogStaff').dialog('open');}"><i class="icon-plus-sign"></i> <?php echo Lang::t('New Custome report') ?></a><?php endif; ?>
                </div>
        </div>
        <div class="col-sm-6">
                <div class="dataTables_filter">
                        <?php
                        $this->beginWidget('ext.activeSearch.AjaxSearch', array(
                            'gridID' => $grid_id,
                            'formID' => $search_form_id,
                            'model' => $model,
                            'action' => Yii::app()->createUrl($this->route, $this->actionParams),
                        ));
                        ?>
                        <?php $this->endWidget(); ?>
                </div>
        </div>
</div>
<?php
$popup = 'function(){$("#perm-frame").attr("src",$(this).attr("href")); $("#dlg-perm-view").dialog("open");  return false;}';
?>

<?php
$this->widget('application.components.widgets.GridView', array(
    'id' => $grid_id,
    'dataProvider' => $model->search(),
    'enablePagination' => $model->enablePagination,
    'enableSummary' => $model->enableSummary,
    'columns' => array(
		'id',
		'name',
                'description',
                'start_date',
                'end_date',
             
             array('class' => 'CButtonColumn',
            'header' => 'Add Products',
            'template' => '{Add}',
             'htmlOptions'=>array('color'=>'green'),
            //--------------------- begin new code --------------------------
            'buttons' => array(
               
                'Add' => array('url' => 'Yii::app()->createUrl("reports/default/add", array("id"=>$data->primaryKey,"gridId"=>$this->grid->id,"name"=>$data->name))',
                     'click' => 'function(){$("#prod-frame").attr("src",$(this).attr("href")); $("#prod-dialog").dialog("open");  return false;}',
                                
                )) ),
             array('class' => 'CButtonColumn',
            'header' => 'View Products',
            'template' => '{Manage}',
             'htmlOptions'=>array('color'=>'green'),
            //--------------------- begin new code --------------------------
            'buttons' => array(
               
                'Manage' => array('url' => 'Yii::app()->createUrl("reports/default/Products", array("id"=>$data->primaryKey,"gridId"=>$this->grid->id,"name"=>$data->name))',
                     'click' => 'function(){$("#perm-frame").attr("src",$(this).attr("href")); $("#dlg-perm-view").dialog("open");  return false;}',
                                
                )) ),
		array(
			'class'=>'CButtonColumn',
                    'template' => '{update} {delete}',
                    'buttons' => array(
                           
                            
                            'update' => array('url' => 'Yii::app()->createUrl("reports/default/update", array("id"=>$data->primaryKey,"gridId"=>$this->grid->id))',
                                'click' => 'function(){$("#groups-frame").attr("src",$(this).attr("href")); $("#groups-dialog").dialog("open");  return false;}',
                                'label' => '<i class="icon-edit bigger-130"></i>',
                                'imageUrl' => false,
                                'visible' => '$this->grid->owner->showLink("' . UserResources::RES_REPORTS_SETUP. '", "' . Acl::ACTION_UPDATE . '")?true:false',
                                'options' => array(
                                'class' => 'green',
                                'title' => Lang::t('Edit details'),
                                )),
                                'delete' =>
                                array(
                                'label' => '<i class="icon-remove bigger-130"></i>',
                                'imageUrl' => false,
                                'visible' => '$this->grid->owner->showLink("' . UserResources::RES_REPORTS_SETUP. '", "' . Acl::ACTION_DELETE . '")?true:false',
                                'options' => array(
                                'class' => 'red',
                                'title' => Lang::t('Delete record'),
                                 'url' => 'Yii::app()->createUrl("reports/default/delete", array("id"=>$data->primaryKey))'
                                )
                            ),
                       
		),
	),
))); ?>


<?php 
//the dialog for editing group permissions
            $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                'id' => 'dlg-perm-view',
                'options' => array(
                    'title' => "Reports Products / Keywords",
                    'autoOpen' => false, //important!
                    'modal' => false,
                    'show'=>array(
                    'effect'=>'fold',
                    'duration'=>800 ),
                    'hide'=>array(
                    'effect'=>'explode',
                    'duration'=>500),
                    'width' => 900,
                    'height' => 500,
                ),
            ));
            ?>
 <iframe id="perm-frame" width="100%" height="100%"></iframe>
            <?php
          $this->endWidget();
          
           /* Dialog for update groups details*/
            $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                'id' => 'groups-dialog',
                'options' => array(
                    'title' => 'Update Custom Reports Details',
                    'autoOpen' => false,
                    'modal' => false,
                    'show'=>array(
                    'effect'=>'fold',
                    'duration'=>800 ),
                    'hide'=>array(
                    'effect'=>'explode',
                    'duration'=>500),
                    'width' => 500,
                    'height' => 550,
                ),
            ));
            ?>
            <iframe id="groups-frame" width="100%" height="100%"></iframe>
            <?php
            $this->endWidget();
            
             
           /* Dialog for update groups details*/
            $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                'id' => 'prod-dialog',
                'options' => array(
                    'title' => 'Add Products',
                    'autoOpen' => false,
                    'modal' => false,
                    'show'=>array(
                    'effect'=>'fold',
                    'duration'=>800 ),
                    'hide'=>array(
                    'effect'=>'explode',
                    'duration'=>500),
                    'width' => 1000,
                    'height' => 750,
                ),
            ));
            ?>
            <iframe id="prod-frame" width="100%" height="100%"></iframe>
            <?php
            $this->endWidget();
            
             

// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogStaff',
    'options' => array(
        'title' => 'Add Custom Report',
        'autoOpen' => false,
        'modal' => false,
        'show'=>array(
                    'effect'=>'fold',
                    'duration'=>800 ),
                    'hide'=>array(
                    'effect'=>'explode',
                    'duration'=>500),
        'width' => 500,
        'height' => 500,
    ),
));
?>
<div class="divStaff"></div>

<?php $this->endWidget(); ?>



<script type="text/javascript">
    // here is the magic
    function addStaff()
    {
<?php
echo CHtml::ajax(array(
    'url' => array("default/create"),
    'data' => "js:$(this).serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogStaff div.divStaff').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#dialogStaff div.divStaff form').submit(addStaff);
                }
                else
                {
                    $('#dialogStaff div.divStaff').html(data.div);
                    setTimeout(\"$('#dialogStaff').dialog('close') \",400);
                                         $('#".$grid_id ."').yiiGridView.update('".$grid_id ."'); 
                }
 
            } ",
))
?>;
        return false;

    }

</script>

<?php
Yii::app()->clientScript->scriptMap['jquery.js'] = true;
$grid_id = 'products-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<!--grid header-->
<div class="row grid-view-header">
    
    <div class="col-sm-6">
        <div class="dataTables_filter">
            <?php
            $this->beginWidget('ext.activeSearch.AjaxSearch', array(
                'gridID' => $grid_id,
                'formID' => $search_form_id,
                'model' => $model,
                'action' => Yii::app()->createUrl($this->route, $this->actionParams),
            ));
            ?>
<?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<?php
$popup = 'function(){$("#perm-frame").attr("src",$(this).attr("href")); $("#dlg-perm-view").dialog("open");  return false;}';
?>

<?php
$this->widget('application.components.widgets.GridView', array(
    'id' => $grid_id,
    'dataProvider' => $model->search(),
    'enablePagination' => $model->enablePagination,
    'enableSummary' => $model->enableSummary,
    'columns' => array(
        'sname',
        'keyword',
        'shortcode',
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' =>
                                array(
                                'url' => 'Yii::app()->Controller->createUrl("default/del", array("id"=>$data->primaryKey, "gridId"=>$this->grid->id))',
                                'label' => '<i class="icon-remove bigger-130"></i>',
                                'imageUrl' => false,
                                'visible' => '$this->grid->owner->showLink("' . UserResources::RES_REPORTS_SETUP. '", "' . Acl::ACTION_DELETE . '")?true:false',
                                'options' => array(
                                'class' => 'red',
                                'title' => Lang::t('Delete record'),
                                 ),
                            ),
            ),
        ),
    ),
));
?>


<?php
// Diaogue for adding new custom report
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogProds',
    'options' => array(
        'title' => 'Add Products for the Report',
        'autoOpen' => false,
        'modal' => true,
        'width' => 400,
        'height' => 500,
    ),
));
?>
<div class="divProds"></div>

<?php $this->endWidget(); ?>



<script type="text/javascript">
    // here is the magic
    function addProds()
    {
<?php
echo CHtml::ajax(array(
    'url' => array("default/add"),
    'data' => "js:$(this).serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(data)
            {
                if (data.status == 'failure')
                {
                    $('#dialogProds div.divProds').html(data.div);
                          // Here is the trick: on submit-> once again this function!
                    $('#dialogProds div.divProds form').submit(addProds);
                }
                else
                {
                    $('#dialogProds div.divProds').html(data.div);
                    setTimeout(\"$('#dialogProds').dialog('close') \",400);
                                         $('#" . $grid_id . "').yiiGridView.update('" . $grid_id . "'); 
                }
 
            } ",
))
?>;
        return false;

    }

</script>


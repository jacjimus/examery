<?php

Yii::app()->clientScript->scriptMap['jquery.js'] = true;
$grid_id = 'prods-grid';
$search_form_id = $grid_id . '-active-search-form';
?>
<!--grid header-->
<div class="row grid-view-header">
    
    <div class="col-sm-6">
        <div class="dataTables_filter">
            <?php
            $this->beginWidget('ext.activeSearch.AjaxSearch', array(
                'gridID' => $grid_id,
                'formID' => $search_form_id,
                'model' => $model,
                'action' => Yii::app()->createUrl($this->route, $this->actionParams),
            ));
            ?>
<?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'email-template-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-vertical',
        'role' => 'form',
    )
        ));

?>
   <div class="form-group">
              <?php
$this->widget('application.components.widgets.GridView', array(
    'id' => $grid_id,
    'dataProvider' => $model_serv->search(),
    'enablePagination' => $model_serv->enablePagination,
    'enableSummary' => $model_serv->enableSummary,
    'columns' => array(
		 array(
                'id' => 'SelectedIds',
                'class' => 'CCheckBoxColumn'
                ),
		'service_name',
                'short_code',
                'keyword',
                
             
	
))); ?>
        </div>
 
<div class="clearfix form-actions">
        <div class="col-lg-offset-2 col-lg-9">
                <button class="btn btn-primary" type="submit"><i class="icon-ok bigger-110"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
        </div>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
function disableme()
{
    $('#mysub').attr("disabled", true);
}
</script>
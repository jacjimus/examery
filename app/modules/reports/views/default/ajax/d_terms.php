<?php
if (isset($_POST)) {
    ?>
<?php
$this->widget('application.extensions.print.printWidget', array(
    'cssFile' => 'print.css',
    'printedElement' => '#printable',
));
?>

<div id="printable">
    <?php
     $start = $_POST['d_date'];
     $short_code = $_POST['d_short_code'] <> "" ? $_POST['d_short_code']: 'All' ;
     $command = Yii::app()->db->createCommand("call sdp.daily_terms('$start' , '$short_code')");
    
  
                
                if ($command)
                    {
 $total = 0;
 $total_gross  = 0;
 $total_net  = 0;
 $new  = 0;
 $unsubs  = 0;
 $terms  = 0;
        
        ?>
        <div style="text-align:left; background-color: white; border: solid 1px #9BB5C1;  padding: 10px;"  title="Reports">
            <h3>Termination Report for <?=$short_code?> short code on  date <?=$_POST['d_date']?></h3><br />
            
            <table width="100%" style="font-size: 14px !important;" >
                <tr height="30px">
                    <td>
                        <b>Subscription Date</b>
                    </td>
                    
                    <td align="left" >
                        <b>Service Name</b>
                    </td>

                    <td style=" text-align: center" >
                        <b>Short Code</b>
                    </td>
                    <td style=" text-align: center" >
                        <b>Cost</b>
                    </td>
                    <td style=" text-align: right" >
                        <b>Terminations</b>
                    </td>
                    <td style=" text-align: right" >
                        <b>Gross Income</b>
                    </td>
                    <td style=" text-align: right" >
                        <b>Net Income</b>
                    </td>
                </tr>
                <?php
                     //   var_dump($sync)
                     foreach($command->queryAll() As $val)
                     {
                        ?>

                        <tr>
                            <td  >
                        <?php
                        echo date("D, d-M-Y", strtotime($val['date']));
                        ?>
                            </td>
                           
                            <td  style="font-weight: 550; text-align: left" >
                                <?php
                                echo $val['service_name'];
                                ?>
                            </td>
                            <td  style="font-weight: 550; text-align: center" >
                                <?php
                                echo $val['short_code'];
                                ?>
                            </td>
                            <td  style="font-weight: 550; text-align: right" >
                                <?php
                                echo number_format($val['cost'] / 100, 2,'.',',');
                                ?>
                            </td>

                            <td  style="font-weight: 550; text-align: right" >
            <?php
                              echo $count = $val['count'];
            ?>
                            </td>
                            <td  style="font-weight: 550; text-align: right" >
                                <?php
                                $gross = $val['cost'] * $count / 100;
                                echo number_format($gross, 2,'.',',');
                                ?>
                            </td>
                            <td  style="font-weight: 550; text-align: right" >
                                <?php
                                $net = ($count * 0.476 * $val['cost'] )  / 100;
                                echo number_format($net, 2,'.',',');
                                ?>
                            </td>
                            

                        </tr>
                        <tr style="height: 0px;">
                            <td colspan="7"><hr></td>
                        </tr>



        <?php 
       
        $new  += $count;
        $total_gross  += $gross;
        $total_net  += $net;
        
                    }
    ?>
                        <tr style="font-weight: bold;">
                            <td style="text-align: right;font-weight: bold; " colspan="4">
                                Totals : 
                            </td>
                            <td style="text-align: right;font-weight: bold;">
                                <?=  number_format($new,0,'.',',')?>
                            </td>
                            <td style="text-align: right;font-weight: bold;">
                                <?=  number_format($total_gross,2,'.',',')?>
                            </td>
                            <td style="text-align: right;font-weight: bold;">
                                <?=  number_format($total_net,2,'.',',')?>
                            </td>
                            
                            
                            
                        </tr>
                </table>

            </div>     <?php
        $this->widget('application.extensions.print.printWidget', array(
            'cssFile' => 'print.css',
            'printedElement' => '#printable',
        ));
        ?>

    <?php
    } 
    else
        echo "<font style='flash'>No records found!</font>";

?>

<script language="javascript" type="text/javascript">

    $(document).ready(function() {
        $(".collapsibleContainer").collapsiblePanel();

    });
</script> 

<?php
}
<?php

/**
 * This is the model class for table "tblreportproducts".
 *
 * The followings are the available columns in table 'tblreportproducts':
 * @property integer $id
 * @property integer $report_id
 * @property integer $service_id
 *
 * The followings are the available model relations:
 * @property Tblcustomereports $report
 */
class Products extends ActiveRecord implements IMyActiveSearch
{
	/**
	 * @return string the associated database table name
	 */
    const USER_RESOURCE_PREFIX = 'PROD_';
        public  $sname;
        public  $keyword;
        public  $shortcode;
        
        
        public function tableName()
	{
		return 'sdp.tblreportproducts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('report_id, service_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, report_id, service_id', 'safe', 'on'=>'search'),
			array('id, report_id, service_id', 'safe', 'on'=>'find'),
		);
	}
          public function afterFind()
        {
                $this->sname = $this->getValue($this->service_id , 'service_name');
                $this->keyword = $this->getValue($this->service_id, 'keyword');
                $this->shortcode = $this->getValue($this->service_id , 'short_code');
                return parent::afterFind();
        }
        public function getValue($id ,  $value)
        {
                if (empty($id))
                        return NULL;
                $val = Services::model()->getScaler($value, '`id`=:t1', array(':t1' => $id));
                return !empty($val) ? $val : NULL;
        }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'report_id' => 'Report',
			'sname' => 'Service Name',
			'service_id' => 'Add Products',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	
	public function searchParams()
        {
                return array(
                    array('id', self::SEARCH_FIELD, true, 'OR'),
                    array('description', self::SEARCH_FIELD, true, 'OR'),
                    'id',
                );
        }

        /**
         * Find email template by key
         * @param type $key
         * @return type
         * @throws CHttpException
         */
        public function findByKey($key)
        {
                $model = $this->find('`key`=:t1', array(':t1' => $key));
                if ($model === null)
                        throw new CHttpException(500, 'No email template with the given key: ' . $key);
                return $model;
        }


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
         public function canBeModified($controller, $type = Acl::ACTION_UPDATE)
        {
                return $controller->showLink(self::USER_RESOURCE_PREFIX . $this->user_level, $type) && Controller::$user_level !== $this->user_level;
        }
}

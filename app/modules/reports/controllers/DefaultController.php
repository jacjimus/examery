<?php


class DefaultController extends ReportsModuleController {

        public function init()
        {
                $this->resource = UserResources::RES_REPORTS;
                 ini_set('max_execution_time', 9000);
                 ini_set("memory_limit",-1);
                $this->resourceLabel = 'Subscription Reports';
                
                parent::init();
        }

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete',
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                    array('allow',
                        'actions' => array('sub', 'term', 'showSubReports', 'showtermreports', 'showweeklytermreports', 'showMonthlyTermReports', 'customTerms',
                                            'weeklySubs', 'monthlySubs', 'CustomSubs', 'setup' , 'create' ,'products' , 'update', 'delete','add' ,'del'),
                        'users' => array('@'),
                    ),
                    array('deny', // deny all users
                        'users' => array('*'),
                    ),
                );
        }

         public function actionSub()
        {
                $this->hasPrivilege(Acl::ACTION_CREATE);
                $this->activeMenu = self::MENU_SUBS;
                $this->pageTitle = Lang::t($this->resourceLabel);

                $model = new Syncorder();
                $model_class_name = $model->getClassName();

                

                $this->render('index', array(
                    'model' => $model,
                ));
        }
        
         public function actionTerm()
        {
                $this->hasPrivilege(Acl::ACTION_CREATE);
                $this->activeMenu = self::MENU_TERM;
                $this->pageTitle = Lang::t(Lang::t("Termination Reports"));

                $model = new Terminations();
                $model_class_name = $model->getClassName();

                

                $this->render('terms', array(
                    'model' => $model,
                ));
        }
        
        public function actionProducts($id , $name)
	{
           $model= Products::model()->searchModel(array(), 6, 'id ASC' , "report_id = '$id'");
               $this->renderPartial('products',array(
			'model'=>$model, 'id' => $id , 'name' => $name
		), false, true);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd($id)
	{
            $this->hasPrivilege(Acl::ACTION_UPDATE);

                $this->pageTitle = Lang::t('Add ' . $this->resourceLabel);
		$model=  new Products();
                 $model_serv = new Services();
               
                $model_class_name = $model->getClassName();
		 // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

    
            if (isset($_POST['SelectedIds']))
                        {
                            
            $ids = $_POST['SelectedIds'];
                        if( $this->splitPosts( $ids, $id ) )
            if ($model->save())
            //$this->redirect(array('view','id'=>$model->PrevID));
            //----- begin new code --------------------
                if (empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("window.parent.$('#prod-dialog').dialog('close');window.parent.$('#prod-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                }
        }

       if (Yii::app()->request->isAjaxRequest)
        {
			//Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        }
        echo $this->renderPartial('_prod', array('model' => $model , 'model_serv' => $model_serv, "id" => $id), false, true);
    }
    
    public function splitPosts($ids , $report)
        {
            $status = true;
            
          
            $count = 1;
            foreach ($ids AS $id)
            {
                 $new = explode(",",$id);
                $model = new Products;
               $model->report_id = $report;
               $model->service_id = $new[0];
               if(!$model->save())
                   $status = false;
                
            }
           
            return $status;
        }
	public function actionCreate()
	{
		$model=new Report;
                
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Report']))
		{
			$model->attributes=$_POST['Report'];
			if($model->save())
                            if (Yii::app()->request->isAjaxRequest)
                {
					Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                    echo CJSON::encode(array(
                        'status'=>'success', 
                        'div'=>"Report added successful"
                        ));
						
                //Close the dialog, reset the iframe and update the grid
               // echo CHtml::script("window.parent.$.fn.yiiGridView.update('#prevExp-grid');");
             
            
                    exit;               
                }
				else
				$this->redirect(array('view','id'=>$model->id));
		}
		
		if (Yii::app()->request->isAjaxRequest)
        {
			Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            echo CJSON::encode(array(
                'status'=>'failure', 
                'div'=>$this->renderPartial('_form', array('model'=>$model),true, true)));
            exit;               
        }
        
	}
        
        public function actionSetup()
	{
		$this->hasPrivilege(Acl::ACTION_VIEW);
                 $this->pageTitle = Lang::t('custome_reports');
               $model= Report::model()->searchModel(array(), $this->settings[Constants::KEY_PAGINATION], 'start_date DESC');
               $model_class_name = $model->getClassName();
               
                        $this->render('setup',array(
			'model'=>$model,
		));
	
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            $this->hasPrivilege(Acl::ACTION_UPDATE);

                $this->pageTitle = Lang::t('Edit ' . $this->resourceLabel);
		$model=  Report::model()->loadModel($id);
 
                $model_class_name = $model->getClassName();
		 // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($model);

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->save())
            //$this->redirect(array('view','id'=>$model->PrevID));
            //----- begin new code --------------------
                if (empty($_GET['asDialog'])) {
                    //Close the dialog, reset the iframe and update the grid
                    echo CHtml::script("window.parent.$('#groups-dialog').dialog('close');window.parent.$('#groups-frame').attr('src','');window.parent.$.fn.yiiGridView.update('{$_GET['gridId']}');");
                    Yii::app()->end();
                }
        }

       if (Yii::app()->request->isAjaxRequest)
        {
			Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        }
        echo $this->renderPartial('_form', array('model' => $model), true, true);
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		Report::model()->loadModel($id)->delete();
   
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	public function actionDel($id)
	{
            
               Products::model()->loadModel($id)->delete();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		 if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array(''));
            }
        
        
        public function actionShowSubReports()
        {
            $this->renderPartial('ajax/daily');
        }
        public function actionShowTermReports()
        {
            $this->renderPartial('ajax/d_terms');
        }
        public function actionWeeklySubs()
        {
            $this->renderPartial('ajax/weekly');
        }
        public function actionShowWeeklyTermReports()
        {
            $this->renderPartial('ajax/w_terms');
        }
        public function actionMonthlySubs()
        {
            $this->renderPartial('ajax/monthly');
        }
        public function actionShowMonthlyTermReports()
        {
            $this->renderPartial('ajax/m_terms');
        }
        public function actionCustomSubs()
        {
            $this->renderPartial('ajax/custome');
        }
        public function actionCustomTerms()
        {
            $this->renderPartial('ajax/c_terms');
        }
         public function getReportName($id)
        {
          if (empty($id))
                        return NULL;
                $dept_id = Report::model()->getScaler('name', '`id`=:t1', array(':t1' => $id));
                return !empty($dept_id) ? $dept_id : NULL; 
        }
}
